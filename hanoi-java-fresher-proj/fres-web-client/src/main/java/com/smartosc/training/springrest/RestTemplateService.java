package com.smartosc.training.springrest;

import com.smartosc.training.dto.request.LoginRequest;
import com.smartosc.training.dto.response.BaseResponse;
import com.smartosc.training.dto.response.PaginatedResponse;
import com.smartosc.training.dto.response.UserResponse;
import com.smartosc.training.exception.APIException;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.EcommerceAPIResponsePagination;
import com.smartosc.training.util.response.JwtAuthenticationResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.Objects;

/**
 * fres-parent
 *
 * @author Administrator
 * @created_at 10/01/2020 - 2:04 PM
 * @created_by Administrator
 * @since 10/01/2020
 */

@Service
public class RestTemplateService {

    @Autowired
    private RestTemplate restTemplate;

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public <T> EcommerceAPIResponsePagination<T> getPaginatedObjects(String url, HttpHeaders headers, ParameterizedTypeReference<EcommerceAPIResponsePagination<T>> type) throws APIException {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            ResponseEntity<EcommerceAPIResponsePagination<T>> response = restTemplate.exchange(url, HttpMethod.GET, entity, type);

            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }
            return response.getBody();

        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    public <T> BaseResponse<T> getObjects(String url, HttpHeaders headers, Map<String, String> filters) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            ParameterizedTypeReference<BaseResponse<T>> type = new ParameterizedTypeReference<BaseResponse<T>>() {
            };
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url, HttpMethod.GET, entity, type,
                    filters);

            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }
            return response.getBody();

        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    public <T> T getObjectsss(String url, HttpMethod method, HttpHeaders headers, Object filters,
                              ParameterizedTypeReference<EcommerceAPIResponse<T>> type) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(filters, headers);
            ResponseEntity<EcommerceAPIResponse<T>> response = restTemplate.exchange(url, method, entity, type);
            if (response.getStatusCodeValue() >= HttpStatus.OK.value()
                    && response.getStatusCodeValue() < HttpStatus.MULTIPLE_CHOICES.value()) {
                return Objects.requireNonNull(response.getBody()).getBody();
            }
            throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
        } catch (Exception e) {
            throw new APIException(e.getMessage(), e);
        }
    }

    public <T> BaseResponse<T> getObject(String url, HttpHeaders headers, Map<String, String> filters) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            ParameterizedTypeReference<BaseResponse<T>> typeRef = new ParameterizedTypeReference<BaseResponse<T>>() {
            };
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url, HttpMethod.GET, entity, typeRef);
            if ((response.getStatusCode().value() < HttpStatus.OK.value())
                    && (response.getStatusCode().value() >= 400)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }

            return response.getBody();
        } catch (Exception e) {

            throw new APIException(e.getMessage());
        }
    }

    public <T> BaseResponse<T> createObject(String url, HttpHeaders headers, Object body) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(body, headers);
            ParameterizedTypeReference<BaseResponse<T>> typeRef = new ParameterizedTypeReference<BaseResponse<T>>() {
            };
            System.out.println(body);
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url, HttpMethod.POST, entity, typeRef);
            if (!response.getStatusCode().equals(HttpStatus.CREATED)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }
            return response.getBody();
        } catch (Exception e) {
            e.printStackTrace();
            throw new APIException(e.getMessage());
        }
    }

    public <T> BaseResponse<T> updateObject(String url, HttpHeaders headers, Object body) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(body, headers);
            ParameterizedTypeReference<BaseResponse<T>> typeRef = new ParameterizedTypeReference<BaseResponse<T>>() {
            };
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url, HttpMethod.PUT, entity, typeRef);
            if (!response.getStatusCode().equals(HttpStatus.CREATED)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }
            return response.getBody();
        } catch (Exception e) {

            throw new APIException(e.getMessage());
        }
    }

    public JwtAuthenticationResponse getJwtToken(String url, LoginRequest loginRequest) {
        try {
            HttpEntity<LoginRequest> entity = new HttpEntity<>(loginRequest);

            ResponseEntity<JwtAuthenticationResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity,
                    JwtAuthenticationResponse.class);
            return response.getBody();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
