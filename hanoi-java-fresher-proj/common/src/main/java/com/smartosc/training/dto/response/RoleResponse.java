package com.smartosc.training.dto.response;

import com.smartosc.training.model.Role;
import com.smartosc.training.util.enumm.RoleName;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RoleResponse implements Serializable {

    private Long id;
    private RoleName role;

    public RoleResponse() {
    }

    public RoleResponse(Role role) {
        this.id = role.getId();
        this.role = role.getName();
    }
}
