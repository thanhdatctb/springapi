package com.smartosc.training.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.smartosc.training.dto.response.BaseResponse;
import com.smartosc.training.dto.response.ProductResponse;
import com.smartosc.training.dto.response.UserResponse;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.springrest.RestTemplateService;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public final class HomeController {

    @Autowired
    private RestTemplateService restTemplateService;

    private ParameterizedTypeReference<EcommerceAPIResponse<ProductResponse>> typeReference =
            new ParameterizedTypeReference<EcommerceAPIResponse<ProductResponse>>() {
            };

    private ParameterizedTypeReference<EcommerceAPIResponse<List<ProductResponse>>> typeReferences =
            new ParameterizedTypeReference<EcommerceAPIResponse<List<ProductResponse>>>() {
            };

    @Value("${services.apiPath}")
    private String baseUrl;

    @GetMapping("/index")
    public String index(Model model) {
        List<ProductResponse> productResponseList = restTemplateService.getObjectsss(baseUrl + "/products",
                HttpMethod.GET, null, null, typeReferences);
        model.addAttribute("productResponseList", productResponseList);
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model, HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getPrincipal().toString().equals("anonymousUser"))
            return "redirect:/index";
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(httpServletRequest, httpServletResponse, auth);
        }
        return "redirect:/login";
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/blog")
    public String blog() {
        return "blog";
    }

    @GetMapping("/contact")
    public String contact() {
        return "contact";
    }

    @GetMapping("/shop")
    public String shop() {
        return "shop";
    }

    @GetMapping("/cart")
    public String cart() {
        return "cart";
    }

    @GetMapping("/checkout")
    public String checkout() {
        return "checkout";
    }

    @GetMapping("/detail/{id}")
    public String detail(Model model, @PathVariable("id") Long id) {
        ProductResponse productResponse = restTemplateService.getObjectsss(baseUrl + "/products/" + id,
                HttpMethod.GET, null, null, typeReference);
        model.addAttribute("products", productResponse);
        List<ProductResponse> productResponseList = restTemplateService.getObjectsss(baseUrl + "/products",
                HttpMethod.GET, null, null, typeReferences);
        model.addAttribute("productResponseList", productResponseList);
        return "product-single";
    }

    @GetMapping("/profile/{id}")
    public String profile(HttpServletRequest request, Model model, @PathVariable("id") Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", SecurityUtils.getJWTToken(request));
        BaseResponse<UserResponse> response = restTemplateService.getObject(baseUrl + "/customer/" + id, headers, null);
        model.addAttribute("user", response.getBody());
        return "user-profile";
    }

}
