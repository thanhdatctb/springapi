package com.smartosc.training.controller;

import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.service.RestTemplateService;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public final class HomeController {

    @Autowired
    private RestTemplateService restTemplateService;

    @Value("${services.path}")
    private String API_URL;

    @Value("${auth}")
    private String auth;

    @GetMapping("/login")
    public String login(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!authentication.getPrincipal().toString().equals("anonymousUser"))
            return "redirect:/index";
        return "auth/login";
    }

    @GetMapping("/index")
    public String index(Model model, HttpServletRequest httpServletRequest) {
        ParameterizedTypeReference<EcommerceAPIResponse<Integer>> typeReference =
                new ParameterizedTypeReference<EcommerceAPIResponse<Integer>>() {
                };
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(auth, SecurityUtils.getJWTToken(httpServletRequest));
        Integer count = restTemplateService.getObjects(API_URL + "/customer/count",
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("count", count);
        Integer countProduct = restTemplateService.getObjects(API_URL + "/products/count",
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("countProduct", countProduct);
        Integer countOrder = restTemplateService.getObjects(API_URL + "/order/count",
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("countOrder", countOrder);
        return "index";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(httpServletRequest, httpServletResponse, auth);
        }
        return "redirect:/";
    }
}
