package com.smartosc.training.util.enumm;

public enum Status {

    ACTIVE(1), DEACTIVE(0);

    private Integer value;

    Status(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
