package com.smartosc.training.dto.response;

import com.smartosc.training.model.OrderDetail;
import com.smartosc.training.util.DateTimeFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderDetailResponse implements Serializable {
    private Long id;
    private Long productId;
    private Integer quantity;
    private float price;
    private String createdAt;
    private String updatedAt;
    private Integer status;
    private OrderResponse orderResponse;
    private ProductResponse productResponse;

    public OrderDetailResponse() {
    }

    public OrderDetailResponse(OrderDetail orderDetail, boolean hasOrder) {
        this.id = orderDetail.getOrder().getId();
        this.productId = orderDetail.getProduct() != null ? orderDetail.getProduct().getId() : null;
        this.quantity = orderDetail.getQuantity();
        this.price = orderDetail.getPrice();
        this.createdAt = DateTimeFormat.formatDateFromLong(orderDetail.getCreatedAt());
        this.updatedAt = DateTimeFormat.formatDateFromLong(orderDetail.getUpdatedAt());
        this.status = orderDetail.getStatus();
        this.productResponse = orderDetail.getProduct() != null ? new ProductResponse(orderDetail.getProduct(), false) : null;
        if (hasOrder) this.orderResponse = new OrderResponse(orderDetail.getOrder(), false);
    }
}
