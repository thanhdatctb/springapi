package com.smartosc.training.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class OrderRequest implements Serializable {

    @JsonProperty("title")
    private String title;

    @JsonProperty("address")
    private String address;

    private List<OrderDetailRequest> list;

    public OrderRequest() {
        this.list = new ArrayList<OrderDetailRequest>();
    }

    @Override
    public String toString() {
        return "OrderRequest [title=" + title + ", address=" + address + ", list=" + list + "]";
    }

}
