package com.smartosc.training.repository;

import com.smartosc.training.model.Order;
import com.smartosc.training.model.OrderDetail;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailRepository extends CustomRepository<OrderDetail, Long> {
    List<OrderDetail> findAllByOrderAndStatus(Order order, Integer status);
}
