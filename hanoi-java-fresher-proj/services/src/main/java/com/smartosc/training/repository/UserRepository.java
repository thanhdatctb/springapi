package com.smartosc.training.repository;

import com.smartosc.training.model.Role;
import com.smartosc.training.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CustomRepository<User, Long> {

    Optional<User> findByUsernameOrPhone(String username, String phone);

    User findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByPhone(String phone);

    Boolean existsByEmail(String email);

    User findByIdAndStatus(Long id, Integer status);

    List<User> findAllByStatusAndRoles(Integer status, Role role);

    @Query("select count(id) from User")
    Integer findCount();

    @Query(value = "SELECT username FROM users", nativeQuery = true)
    List<String> getUsernames();
}
