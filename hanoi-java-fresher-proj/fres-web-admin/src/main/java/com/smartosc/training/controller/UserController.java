package com.smartosc.training.controller;

import com.smartosc.training.dto.request.CurrentUserRequest;
import com.smartosc.training.dto.response.UserResponse;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.service.RestTemplateService;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * fres-parent
 *
 * @author Lam Chuot
 * @created_at 03/02/2020 - 10:26
 * @created_by Lam Chuot
 * @since 03/02/2020
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {

    private HttpHeaders httpHeaders = new HttpHeaders();

    private ParameterizedTypeReference<EcommerceAPIResponse<UserResponse>> typeReference =
            new ParameterizedTypeReference<EcommerceAPIResponse<UserResponse>>() {
            };

    @Value("${services.path}" + "/customer")
    private String API_URL;

    @Value("${auth}")
    private String auth;

    @Autowired
    private RestTemplateService restTemplateService;

    @GetMapping
    public String list(Model model) {
        ParameterizedTypeReference<EcommerceAPIResponse<List<UserResponse>>> typeReferences =
                new ParameterizedTypeReference<EcommerceAPIResponse<List<UserResponse>>>() {
                };
        List<UserResponse> userResponseList = restTemplateService.getObjects(API_URL,
                HttpMethod.GET, null, null, typeReferences);
        model.addAttribute("userResponseList", userResponseList);
        return "user/data";
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        UserResponse userResponse = restTemplateService.getObjects(API_URL + "/" + id,
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("user", userResponse);
        return "detail";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
        if (id == null) {
            return "Fail";
        }
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        UserResponse user = restTemplateService.getObjects(API_URL + "/" + id,
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("user", user);
        return "user/edit";
    }

    @PostMapping("/edit/{id}")
    public String edit(Model model, @ModelAttribute("user") UserResponse user,
                       @PathVariable("id") Long id,
                       HttpServletRequest request,
                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("user", user);
            return "user/edit";
        }
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        restTemplateService.getObjects(API_URL + "/" + id, HttpMethod.PUT, httpHeaders, user, typeReference);
        return "redirect:/user";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, CurrentUserRequest user, HttpServletRequest request) {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        restTemplateService.getObjects(API_URL + "/" + id, HttpMethod.DELETE, httpHeaders, user, typeReference);
        return "redirect:/user";
    }

}
