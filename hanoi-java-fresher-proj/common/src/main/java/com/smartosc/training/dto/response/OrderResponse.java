package com.smartosc.training.dto.response;

import com.smartosc.training.model.Order;
import com.smartosc.training.util.DateTimeFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class OrderResponse implements Serializable {
    private Long id;
    private Long userId;
    private String fullName;
    private String title;
    private String address;
    private String phone;
    private float totalPrice;
    private Integer status;
    private String createdAt;
    private String updatedAt;

    private List<OrderDetailResponse> responses = new ArrayList<>();

    public OrderResponse(Order order, boolean hasOrderDetail) {
        this.id = order.getId();
        this.userId = order.getUser().getId();
        this.fullName = order.getUser().getFullName();
        this.title = order.getTitle();
        this.address = order.getAddress();
        this.phone = order.getUser().getPhone();
        this.totalPrice = order.getTotalPrice();
        this.status = order.getStatus();
        this.createdAt = DateTimeFormat.formatDateFromLong(order.getCreatedAt());
        this.updatedAt = DateTimeFormat.formatDateFromLong(order.getUpdatedAt());
        if (hasOrderDetail)
            this.responses = order.getOrderDetails().stream()
                    .map(orderDetail -> new OrderDetailResponse(orderDetail, false)).collect(Collectors.toList());
    }

    public OrderResponse() {

    }

    @Override
    public String toString() {
        return "OrderResponse [id=" + id + ", userId=" + userId + ", fullName=" + fullName + ", title=" + title
                + ", address=" + address + ", phone=" + phone + ", totalPrice=" + totalPrice + ", status=" + status
                + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", responses=" + responses + "]";
    }

}
