package com.smartosc.training.service;

import com.smartosc.training.dto.request.OrderDetailRequest;
import com.smartosc.training.dto.request.OrderRequest;
import com.smartosc.training.dto.response.OrderDetailResponse;
import com.smartosc.training.dto.response.OrderResponse;
import com.smartosc.training.model.*;
import com.smartosc.training.repository.OrderDetailRepository;
import com.smartosc.training.repository.OrderRepository;
import com.smartosc.training.repository.ProductRepository;
import com.smartosc.training.util.enumm.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Order> getAllOrder() {
        return orderRepository.findAllByOrderByCreatedAtDesc();
    }

    @Override
    public List<Order> getAllByUser(User user) {
        return null;
    }

    @Override
    public Page<Order> getAllByUserWithPaginate(Specification specification, int page, int limit) {
        return orderRepository.findAll(specification, PageRequest.of(page - 1, limit));
    }

    @Override
    public Order updateOrder(Long id, OrderRequest orderRequest) {
        Order saveOrder = orderRepository.getOne(id);
        float totalPrice = 0;

        Set<OrderDetail> orderDetails = new HashSet<>();
        for (OrderDetailRequest orderDetailRequest : orderRequest.getList()) {
            OrderDetail orderDetailCurrent = null;
            if (orderDetailRequest.getProductId() != null) {
                Product product = productRepository.findByIdAndStatus(orderDetailRequest.getProductId(),
                        Status.ACTIVE.getValue());
                if (product == null)
                    return null;
                orderDetailCurrent = new OrderDetail(saveOrder, product, orderDetailRequest.getQuantity(),
                        product.getPrice());
            }

            OrderDetail orderDetail = orderDetailRepository.save(orderDetailCurrent);
            orderDetails.add(orderDetail);
            totalPrice += orderDetailCurrent.getPrice() * orderDetailRequest.getQuantity();
        }

        saveOrder.setTotalPrice(totalPrice);
        saveOrder.setOrderDetails(orderDetails);
        return orderRepository.save(saveOrder);

    }

    @Override
    public Order saveOrderByUser(User user, OrderRequest orderRequest) {
        Order order = new Order((float) 0, orderRequest.getTitle(), orderRequest.getAddress(), user);
        Order saveOrder = orderRepository.save(order);
        float totalPrice = 0;

        Set<OrderDetail> orderDetails = new HashSet<>();
        for (OrderDetailRequest orderDetailRequest : orderRequest.getList()) {
            OrderDetail orderDetailCurrent = null;
            if (orderDetailRequest.getProductId() != null) {
                Product product = productRepository.findByIdAndStatus(orderDetailRequest.getProductId(),
                        Status.ACTIVE.getValue());
                if (product == null)
                    return null;
                orderDetailCurrent = new OrderDetail(saveOrder, product, orderDetailRequest.getQuantity(),
                        product.getPrice());
            }

            OrderDetail orderDetail = orderDetailRepository.save(orderDetailCurrent);
            orderDetails.add(orderDetail);
            totalPrice += orderDetailCurrent.getPrice() * orderDetailRequest.getQuantity();
        }

        saveOrder.setTotalPrice(totalPrice);
        saveOrder.setOrderDetails(orderDetails);
        return orderRepository.save(saveOrder);
    }

    @Override
    public Order updateStatusOrder(Long id, Integer status) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (orderOptional.isPresent()) {
            Order order = orderOptional.get();
            if (order.getStatus() <= 5)
                order.setStatus(status);
            orderRepository.save(order);
            return order;
        }
        return null;
    }

    @Override
    public OrderResponse getById(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(null);
        if (order == null) {
            return null;
        }

        List<OrderDetail> orderDetails = orderDetailRepository.findAllByOrderAndStatus(order, Status.ACTIVE.getValue());
        Set<OrderDetailResponse> detailResponses = new HashSet<>();
        for (OrderDetail orderDetail : orderDetails) {
            OrderDetailResponse onlyOrderDetailResponse = new OrderDetailResponse(orderDetail, true);
            detailResponses.add(onlyOrderDetailResponse);
        }

        return new OrderResponse(order, true);
    }

    @Override
    public Integer findCount() {
        return orderRepository.findCount();
    }

    @Override
    public List<Order> getAllByCreatedAtBetween(String from, String to) {
        Instant fromInstant = DateAudit.stringToInstant(from);
        Instant toInstant = DateAudit.stringToInstant(to);
        return orderRepository.findAllByCreatedAtBetween(fromInstant, toInstant);
    }

    @Override
    public List<Order> getByStatus(User user, Integer status) {
        return orderRepository.findByStatus(user.getId(), status);
    }
}
