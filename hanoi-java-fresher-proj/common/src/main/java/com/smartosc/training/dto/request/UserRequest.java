package com.smartosc.training.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smartosc.training.util.annotation.EmailConstraint;
import com.smartosc.training.util.annotation.FieldMatch;
import com.smartosc.training.util.annotation.NotContainSpecialCharacter;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@FieldMatch.List({@FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match")})
public class UserRequest {

    @NotNull
    @JsonProperty("full_name")
    @NotContainSpecialCharacter
    private String fullName;

    @NotNull
    @Size(min = 4, max = 50)
    @JsonProperty("username")
    @NotContainSpecialCharacter
    private String username;

    @NotNull
    @EmailConstraint
    @JsonProperty("email")
    @Size(min = 6, max = 64)
    private String email;

    @NotNull
    @JsonProperty("phone")
    private String phone;

    @NotNull
    @Size(min = 6, max = 32)
    @JsonProperty("password")
    private String password;

    @NotNull
    @Size(min = 6, max = 32)
    @JsonProperty("confirm_password")
    private String confirmPassword;

    @Override
    public String toString() {
        return "UserRequest [fullName=" + fullName + ", username=" + username + ", email=" + email + ", phone=" + phone
                + ", password=" + password + "]";
    }


}
