package com.smartosc.training.model;

import javax.persistence.*;

import com.smartosc.training.util.enumm.Status;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "order_detail")
@Setter
@Getter
public class OrderDetail extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price", precision = 2)
    private float price;

    @Column(name = "status")
    private Integer status;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    public OrderDetail() {
    }

    public OrderDetail(Order order, Product product, Integer quantity, float price) {
        this.order = order;
        this.product = product;
        this.quantity = quantity;
        this.price = price;
        this.status = Status.ACTIVE.getValue();
    }
}