package com.smartosc.training.dto.response;

import com.smartosc.training.model.Category;
import com.smartosc.training.util.DateTimeFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * fres-parent
 *
 * @author DuongTrong
 * @created_at 10/01/2020 - 1:45 PM
 * @created_by DuongTrong
 * @since 10/01/2020
 */

@Getter
@Setter
public class CategoryResponse implements Serializable {

    private Long id;
    private String name;
    private String createdAt;
    private String updatedAt;
    private Integer status;
    private List<ProductResponse> products = new ArrayList<>();

    public CategoryResponse() {
    }

    public CategoryResponse(Category category, boolean hasProduct) {
        this.id = category.getId();
        this.name = category.getName();
        this.createdAt = DateTimeFormat.formatDateFromLong(category.getCreatedAt());
        this.updatedAt = DateTimeFormat.formatDateFromLong(category.getUpdatedAt());
        this.status = category.getStatus();
        if (hasProduct)
            this.products = category.getProducts().stream().map(x -> new ProductResponse(x, false)).collect(Collectors.toList());
    }

}
