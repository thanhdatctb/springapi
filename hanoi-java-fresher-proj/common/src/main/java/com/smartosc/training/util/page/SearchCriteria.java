package com.smartosc.training.util.page;

import lombok.Getter;
import lombok.Setter;

/**
 * hanoi-java-fresher-proj
 *
 * @author DuongTrong
 * @created_at 09/01/2020 - 2:37 PM
 * @created_by DuongTrong
 * @since 09/01/2020
 */

@Getter
@Setter
public class SearchCriteria {

    private String key;
    private String operation;
    private Object value;

    public SearchCriteria(final String key, final String operation, final Object value) {
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

}
