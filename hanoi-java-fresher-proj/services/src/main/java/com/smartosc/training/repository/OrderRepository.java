package com.smartosc.training.repository;

import com.smartosc.training.model.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface OrderRepository extends CustomRepository<Order, Long> {

    List<Order> findAllByCreatedAtBetween(Instant form, Instant to);

    @Query("SELECT o from Order o ORDER BY o.createdAt DESC ")
    List<Order> findAllByOrderByCreatedAtDesc();

    @Query("SELECT o from Order o where user_id = :userId and status = :status")
    List<Order> findByStatus(Long userId, Integer status);

    @Query("select count(id) from Order")
    Integer findCount();
}
