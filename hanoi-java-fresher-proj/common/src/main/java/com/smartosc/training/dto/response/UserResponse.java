package com.smartosc.training.dto.response;

import com.smartosc.training.model.User;
import com.smartosc.training.util.DateTimeFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class UserResponse implements Serializable {
    private Long id;
    private String fullName;
    private String username;
    private String email;
    private String phone;
    private String createdAt;
    private String updatedAt;
    private Integer status;
    private List<RoleResponse> roles = new ArrayList<>();

    public UserResponse() {
    }

    public UserResponse(User user, boolean hasRole) {
        this.id = user.getId();
        this.fullName = user.getFullName();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.createdAt = DateTimeFormat.formatDateFromLong(user.getCreatedAt());
        this.updatedAt = DateTimeFormat.formatDateFromLong(user.getUpdatedAt());
        this.status = user.getStatus();
        if (hasRole) this.roles = user.getRoles().stream().map(RoleResponse::new).collect(Collectors.toList());
    }
}
