package com.smartosc.training.dto.response;

import com.smartosc.training.util.response.APIResponseDataPagination;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaginatedResponse<T> extends BaseResponse<T> {
    private APIResponseDataPagination pagination;
}
