package com.smartosc.training.util.enumm;

public enum RoleName {
    ROLE_USER, ROLE_ADMIN;
}
