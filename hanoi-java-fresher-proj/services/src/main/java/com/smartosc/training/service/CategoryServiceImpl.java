package com.smartosc.training.service;

import com.smartosc.training.dto.request.CategoryRequest;
import com.smartosc.training.exception.CustomRuntimeException;
import com.smartosc.training.model.Category;
import com.smartosc.training.repository.CategoryRepository;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * fres-parent
 *
 * @author Administrator
 * @created_at 09/01/2020 - 12:03 PM
 * @created_by Administrator
 * @since 09/01/2020
 */

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAllByStatusOrderByCreatedAtDesc(Status.ACTIVE.getValue());
    }

    @Override
    public Category getCategory(Long id) {
        if (CollectionUtils.isEmpty(Collections.singleton(id))) {
            throw new CustomRuntimeException(MessageConstants.NULL_POINTER);
        }
        return categoryRepository.findByIdAndStatus(id, Status.ACTIVE.getValue());
    }

    @Override
    public Category addCategoryOrUpdate(Category category, CategoryRequest categoryRequest) {
        if (categoryRequest.getName() != null) category.setName(categoryRequest.getName());
        if (category.getStatus() == null) category.setStatus(Status.ACTIVE.getValue());
        return categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(Category category) {
        category.setStatus(Status.DEACTIVE.getValue());
        categoryRepository.save(category);
    }

    @Override
    public List<Category> findAllByIdIn(Collection<Long> ids) {
        return categoryRepository.findAllByIdIn(ids);
    }

    @Override
    public Page<Category> categoriesWithPaginate(Specification specification, int page, int limit) {
        return categoryRepository.findAll(specification, PageRequest.of(page - 1, limit));
    }
}
