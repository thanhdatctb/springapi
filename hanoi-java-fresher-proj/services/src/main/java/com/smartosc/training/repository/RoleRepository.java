package com.smartosc.training.repository;

import com.smartosc.training.model.Role;
import com.smartosc.training.util.enumm.RoleName;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CustomRepository<Role, Long> {

    Role findByName(RoleName name);
}
