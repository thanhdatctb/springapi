package com.smartosc.training.exception;

/**
 * hanoi-java-fresher-proj
 *
 * @author DuongTrong
 * @created_at 09/01/2020 - 3:01 PM
 * @created_by DuongTrong
 * @since 09/01/2020
 */

public class CustomRuntimeException extends RuntimeException {

    public CustomRuntimeException(String message) {
        super(message);
    }

    public CustomRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
