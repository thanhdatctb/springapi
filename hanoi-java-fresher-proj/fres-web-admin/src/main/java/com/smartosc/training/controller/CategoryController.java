package com.smartosc.training.controller;

import com.smartosc.training.dto.request.CategoryRequest;
import com.smartosc.training.dto.response.CategoryResponse;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.service.RestTemplateService;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoryController {

    private HttpHeaders httpHeaders = new HttpHeaders();

    private ParameterizedTypeReference<EcommerceAPIResponse<CategoryResponse>> typeReference =
            new ParameterizedTypeReference<EcommerceAPIResponse<CategoryResponse>>() {
            };

    @Value("${services.path}" + "/categories")
    private String URL_API;

    @Value("${auth}")
    private String auth;

    @Autowired
    private RestTemplateService restTemplateService;

    @GetMapping
    public String list(Model model) {
        ParameterizedTypeReference<EcommerceAPIResponse<List<CategoryResponse>>> typeReferences =
                new ParameterizedTypeReference<EcommerceAPIResponse<List<CategoryResponse>>>() {
                };
        List<CategoryResponse> categoryResponses = restTemplateService.getObjects(URL_API,
                HttpMethod.GET, null, null, typeReferences);
        model.addAttribute("categoryResponses", categoryResponses);
        return "category/data";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute("categoryRequest") CategoryRequest categories, HttpServletRequest request, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "category/create";
        }
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        restTemplateService.getObjects(URL_API + "/create", HttpMethod.POST, httpHeaders, categories, typeReference);
        return "redirect:/categories";
    }

    @GetMapping("/create")
    public String createO(@ModelAttribute("categories") CategoryRequest categories, Model model, HttpServletRequest request) {
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        model.addAttribute("categories", categories);
        return "category/create";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
        if (id == null) {
            return "Lỗi rồi";
        }
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        CategoryResponse categories = restTemplateService.getObjects(URL_API + "/" + id,
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("categories", categories);
        return "category/edit";
    }

    @PostMapping("/edit/{id}")
    public String editO(@ModelAttribute("categories") CategoryRequest categories, @PathVariable("id") Long id, HttpServletRequest request) {
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        restTemplateService.getObjects(URL_API + "/" + id, HttpMethod.PUT, httpHeaders, categories, typeReference);
        return "redirect:/categories";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, CategoryRequest categoryRequest, HttpServletRequest request) {
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        restTemplateService.getObjects(URL_API + "/" + id, HttpMethod.DELETE, httpHeaders, categoryRequest, typeReference);
        return "redirect:/categories";
    }
}
