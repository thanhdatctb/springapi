package com.smartosc.training.util.enumm;

public enum OrderStatus {
    IN_CART(0), SUBMITTED(1), IN_DELIVERY(2), COMPLETED(3);

    private Integer value;

    OrderStatus(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return this.value;
    }
}