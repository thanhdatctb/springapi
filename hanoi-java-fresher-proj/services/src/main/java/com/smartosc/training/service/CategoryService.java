package com.smartosc.training.service;

import com.smartosc.training.dto.request.CategoryRequest;
import com.smartosc.training.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.Collection;
import java.util.List;

/**
 * fres-parent
 *
 * @author Administrator
 * @created_at 09/01/2020 - 12:03 PM
 * @created_by Administrator
 * @since 09/01/2020
 */

public interface CategoryService {

    List<Category> getAllCategories();

    Category getCategory(Long id);

    Category addCategoryOrUpdate(Category category, CategoryRequest categoryRequest);

    void deleteCategory(Category category);

    List<Category> findAllByIdIn(Collection<Long> ids);

    Page<Category> categoriesWithPaginate(Specification specification, int page, int limit);
}
