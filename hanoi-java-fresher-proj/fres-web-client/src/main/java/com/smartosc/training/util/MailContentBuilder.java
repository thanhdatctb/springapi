package com.smartosc.training.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.WebContext;

import com.smartosc.training.dto.request.UserRequest;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;
    private ServletContext servlContext;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine, ServletContext servlContext) {
        this.templateEngine = templateEngine;
        this.servlContext = servlContext;
    }

    public String build(String mailTemplate, Map<String, Object> variables) {
        Context context = new Context();
        for (Entry<String, Object> variable : variables.entrySet()) {
            context.setVariable(variable.getKey(), variable.getValue());
        }
        return templateEngine.process(mailTemplate, context);
    }

    public String buildWebContext(String mailTemplate, Map<String, Object> variables, HttpServletRequest request,
                                  HttpServletResponse response) {
        WebContext context = new WebContext(request, response, servlContext);
        for (Entry<String, Object> variable : variables.entrySet()) {
            context.setVariable(variable.getKey(), variable.getValue());
        }
        return templateEngine.process(mailTemplate, context);

    }

    public String buildRegisterConfirmationTemplate(String email, String confirmationUrl) {
        Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("email", email);
        variables.put("link", confirmationUrl);
        return build("mailTemplate/registerConfirmation", variables);
    }
}
