package com.smartosc.training.services;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class SMTPService {

    @Autowired
    private JavaMailSender javaMail;

    public void sendEmail(String[] usernames, String[] cc, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(usernames);
        if (null != cc && cc.length > 0) {
            message.setCc(cc);
        }
        message.setSubject(subject);
        message.setText(body);
        javaMail.send(message);
    }

    public void sendEmailWithAttachment(String[] usernames, String[] cc, String subject, String body, File attachment)
            throws MessagingException {
        MimeMessage msg = javaMail.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(usernames);
        if (cc != null && cc.length > 0) {
            helper.setCc(cc);
        }
        helper.setText(body);
        helper.addAttachment(attachment.getName(), attachment);
    }
}
