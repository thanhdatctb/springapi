package com.smartosc.training.util.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Method;

/**
 * hanoi-java-fresher-proj
 *
 * @author DuongTrong
 * @created_at 09/01/2020 - 2:50 PM
 * @created_by DuongTrong
 * @since 09/01/2020
 */

public class PasswordsEqualConstraintValidator implements
        ConstraintValidator<PasswordsEqualConstraint, Object> {

    @Override
    public void initialize(PasswordsEqualConstraint arg0) {
    }

    @Override
    public boolean isValid(Object candidate, ConstraintValidatorContext context) {
        try {
            Method methodGetPassword = candidate.getClass().getMethod("getPassword");
            Method methodGetConfirmPassword = candidate.getClass().getMethod("getConfirmPassword");
            if (methodGetPassword.invoke(candidate) == null && methodGetConfirmPassword.invoke(candidate) == null)
                return true;
            else if (methodGetPassword.invoke(candidate) == null)
                return false;
            return methodGetPassword.invoke(candidate).equals(methodGetConfirmPassword.invoke(candidate));
        } catch (Exception noSuchMethodException) {
            noSuchMethodException.printStackTrace();
            return false;
        }
    }
}
