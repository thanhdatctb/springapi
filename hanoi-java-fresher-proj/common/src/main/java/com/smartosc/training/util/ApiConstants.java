package com.smartosc.training.util;

public class ApiConstants {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    public static final String ROLE_USER = "ROLE_USER";

    public static final String[] PUBLIC_HTML = {
            "/",
            "/favicon.ico",
            "/**/*.png",
            "/**/*.gif",
            "/**/*.svg",
            "/**/*.jpg",
            "/**/*.html",
            "/**/*.css",
            "/**/*.js"
    };

    public static final String[] PRIVATE_ADMIN = {
            "/api/v2/auth/admin/login",
            "/api/v1/order/all"
    };

    public static final String[] PRIVATE_URL = {
            "/api/v1/customer/**",
            "/api/v1/products/**",
            "/api/v1/categories/**"
    };

    public static final String[] GET_PUBLIC_CLIENT = {
            "/api/v2/auth/admin/**",
            "/api/v1/auth/login",
            "/api/v1/auth/register",
            "/api/v1/products/**",
            "/api/v1/categories/**",
            "/api/v1/customer/usernames",
            "/api/v1/customer/{id}",
            "/api/v1/customer"
    };

}
