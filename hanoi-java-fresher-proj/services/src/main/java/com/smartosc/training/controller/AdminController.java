package com.smartosc.training.controller;

import com.smartosc.training.dto.request.LoginRequest;
import com.smartosc.training.dto.request.UserRequest;
import com.smartosc.training.dto.response.UserResponse;
import com.smartosc.training.model.User;
import com.smartosc.training.security.JwtTokenProvider;
import com.smartosc.training.service.UserService;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.RoleName;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.JwtAuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/auth/admin")
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/register")
    public ResponseEntity<Object> createAdmin(@Valid @RequestBody UserRequest userRequest) {
        User user = new User();
        User current = userService.registerAdmin(user, userRequest);
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.CREATED.value(),
                        MessageConstants.CREATE_SUCCESS, new UserResponse(current, true)), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> loginAdmin(@Valid @RequestBody LoginRequest loginRequest) {
        Optional<User> user = userService.findByUsernameOrPhone(loginRequest.getAccount(), loginRequest.getAccount());
        if (user.isPresent() && userService.checkRoleByUser(user.get(), RoleName.ROLE_ADMIN)) {
            User current = user.get();
            if (!passwordEncoder.matches(loginRequest.getPassword(), current.getPassword())) {
                return new ResponseEntity<>(
                        new EcommerceAPIResponse<>(
                                HttpStatus.UNAUTHORIZED.value(),
                                MessageConstants.PASSWORD_INCORRECT),
                        HttpStatus.UNAUTHORIZED);
            }
        } else {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(
                    HttpStatus.UNAUTHORIZED.value(),
                    MessageConstants.ACCOUNT_ACCESS_DENIED),
                    HttpStatus.UNAUTHORIZED);
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getAccount(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);

        return new ResponseEntity<>(new JwtAuthenticationResponse(HttpStatus.OK.value(), MessageConstants.HELLO_BOSS, jwt), HttpStatus.OK);
    }
}
