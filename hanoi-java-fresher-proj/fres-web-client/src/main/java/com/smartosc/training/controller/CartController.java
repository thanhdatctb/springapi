package com.smartosc.training.controller;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import com.smartosc.training.dto.request.OrderDetailRequest;
import com.smartosc.training.dto.request.OrderRequest;
import com.smartosc.training.dto.response.BaseResponse;
import com.smartosc.training.dto.response.OrderResponse;
import com.smartosc.training.dto.response.ProductResponse;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.springrest.RestTemplateService;
import com.smartosc.training.util.enumm.OrderStatus;

@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private ModelMapper modelMapper;

    @Value("${services.apiPath}" + "/order")
    private String apiPath;

    private HttpHeaders headers = new HttpHeaders();

    @PostMapping("/add")
    public String addToCart(HttpServletRequest request, @RequestBody OrderDetailRequest product, Model model) {
//		get cart
        try {
            headers.add("Authorization", SecurityUtils.getJWTToken(request));
            BaseResponse<OrderResponse> cartResponse = restTemplateService.getObject(apiPath, headers, null);
            OrderResponse oldCart = modelMapper.map(cartResponse.getBody(), OrderResponse.class);
            OrderRequest cartUpdateRequest = modelMapper.map(cartResponse.getBody(), OrderRequest.class);
//			insert to cart
            cartUpdateRequest.getList().add(product);
            apiPath += "/" + oldCart.getId();
            BaseResponse<OrderResponse> requestResponse = restTemplateService.updateObject(apiPath, headers,
                    cartUpdateRequest);
            OrderResponse cart = modelMapper.map(requestResponse.getBody(), OrderResponse.class);
            model.addAttribute("cart", cart);
            return "cartDetail";
        } catch (Exception e) {
            e.getStackTrace();
        }
        return null;
    }

    @GetMapping("/")
    public String viewCart(HttpServletRequest request, Model model) {
        headers.add("Authorization", SecurityUtils.getJWTToken(request));
        System.out.println(SecurityUtils.getJWTToken(request));
        BaseResponse<OrderResponse> cartResponse = restTemplateService.getObject(apiPath, headers, null);
        OrderResponse cart = modelMapper.map(cartResponse.getBody(), OrderResponse.class);
        model.addAttribute("cart", cart);
        return "cartDetail";

    }

    @PostMapping("/empty")
    public String emptyCart(HttpServletRequest request, Model model) {
        String url = apiPath + "/order";
//		get cart
        headers.add("Authorization", SecurityUtils.getJWTToken(request));
        BaseResponse<OrderResponse> cartResponse = restTemplateService.getObject(apiPath, headers, null);
        OrderResponse oldCart = modelMapper.map(cartResponse.getBody(), OrderResponse.class);
        OrderRequest cartUpdateRequest = modelMapper.map(cartResponse.getBody(), OrderRequest.class);
//			empty cart
        cartUpdateRequest.setList(new ArrayList<OrderDetailRequest>());
        apiPath += "/" + oldCart.getId();
        BaseResponse<OrderResponse> requestResponse = restTemplateService.updateObject(apiPath, headers, cartUpdateRequest);
        OrderResponse cart = modelMapper.map(requestResponse.getBody(), OrderResponse.class);
        System.out.println("API: " + cart);
        model.addAttribute("cart", cart);
        return "cartDetail";
    }

    @PutMapping("/update")
    public String updateCart(HttpServletRequest request, @RequestBody OrderRequest order, Model model) {
        try {
            headers.add("Authorization", SecurityUtils.getJWTToken(request));
            BaseResponse<OrderResponse> cartResponse = restTemplateService.getObject(apiPath, headers, null);
            OrderRequest cartUpdateRequest = modelMapper.map(cartResponse.getBody(), OrderRequest.class);
            cartUpdateRequest.setList(order.getList());
            BaseResponse<OrderResponse> requestResponse = restTemplateService.updateObject(apiPath, headers,
                    cartUpdateRequest);
            OrderResponse cart = modelMapper.map(requestResponse.getBody(), OrderResponse.class);
            model.addAttribute("cart", cart);
            return "cartDetail";

        } catch (Exception e) {
            e.getStackTrace();
        }
        return null;
    }

    @PostMapping("/purchase/{id}")
    public String purchase(HttpServletRequest request, @PathVariable(value = "id") Long id, Model model) {
        String url = apiPath + "/status/" + id + "?status=" + OrderStatus.SUBMITTED.getValue();
        headers.add("Authorization", SecurityUtils.getJWTToken(request));
        BaseResponse<OrderResponse> purchaseResponse = restTemplateService.updateObject(url, headers, null);
//		return "index";
        OrderResponse cart = modelMapper.map(purchaseResponse.getBody(), OrderResponse.class);
        model.addAttribute("cart", cart);
        return "cartDetail";

    }
}
