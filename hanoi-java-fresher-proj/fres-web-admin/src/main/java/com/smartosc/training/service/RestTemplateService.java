package com.smartosc.training.service;

import java.util.Map;
import java.util.Objects;

import com.smartosc.training.dto.request.LoginRequest;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.EcommerceAPIResponsePagination;
import com.smartosc.training.util.response.JwtAuthenticationResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.smartosc.training.dto.response.BaseResponse;
import com.smartosc.training.exception.APIException;

@Service
public class RestTemplateService {

    @Autowired
    private RestTemplate restTemplate;

    public <T> EcommerceAPIResponsePagination<T> getPaginatedObjects(String url, HttpHeaders headers, ParameterizedTypeReference<EcommerceAPIResponsePagination<T>> type) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            ResponseEntity<EcommerceAPIResponsePagination<T>> response = restTemplate.exchange(url, HttpMethod.GET, entity, type);

            if (!response.getStatusCode().equals(HttpStatus.OK)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }
            return response.getBody();

        } catch (Exception e) {
            throw new APIException(e.getMessage());
        }
    }

    public <T> T getObjects(String url, HttpMethod method, HttpHeaders headers, Object filters,
                            ParameterizedTypeReference<EcommerceAPIResponse<T>> type) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(filters, headers);
            ResponseEntity<EcommerceAPIResponse<T>> response = restTemplate.exchange(url, method, entity, type);
            if (response.getStatusCodeValue() >= HttpStatus.OK.value() && response.getStatusCodeValue() < HttpStatus.MULTIPLE_CHOICES.value()) {
                return Objects.requireNonNull(response.getBody()).getBody();
            }
            throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
        } catch (Exception e) {
            throw new APIException(e.getMessage(), e);
        }
    }

    public <T> BaseResponse<T> getObject(String url, HttpHeaders headers, Map<String, String> filters,
                                         Class<BaseResponse<T>> type) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url, HttpMethod.GET, entity, type,
                    filters);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }
            return response.getBody();
        } catch (Exception e) {

            throw new APIException(e.getMessage());
        }
    }

    public <T> BaseResponse<T> createObject(String url, HttpHeaders headers, Object body, Class<BaseResponse<T>> type) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(body, headers);
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url, HttpMethod.POST, entity, type);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                throw new APIException(Objects.requireNonNull(response.getBody()).getMessage());
            }
            return response.getBody();
        } catch (Exception e) {

            throw new APIException(e.getMessage());
        }
    }

    public <T> BaseResponse<T> updateObject(String url, HttpHeaders headers, Object body, ParameterizedTypeReference<BaseResponse<T>> reference) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(body, headers);
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url, HttpMethod.PUT, entity, reference);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                throw new APIException(response.getBody().getMessage());
            }
            return response.getBody();
        } catch (Exception e) {

            throw new APIException(e.getMessage());
        }
    }

    public <T> BaseResponse<T> deleteObject(String url, HttpHeaders headers, int id, Class<BaseResponse<T>> type) {
        try {
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            ResponseEntity<BaseResponse<T>> response = restTemplate.exchange(url + "/" + id, HttpMethod.DELETE, entity,
                    type);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                throw new APIException(response.getBody().getMessage());
            }
            return response.getBody();
        } catch (Exception e) {

            throw new APIException(e.getMessage());
        }
    }

    public JwtAuthenticationResponse getJwtToken(String url, LoginRequest loginRequest) {
        try {
            HttpEntity<LoginRequest> entity = new HttpEntity<>(loginRequest);

            ResponseEntity<JwtAuthenticationResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity,
                    JwtAuthenticationResponse.class);
            return response.getBody();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
