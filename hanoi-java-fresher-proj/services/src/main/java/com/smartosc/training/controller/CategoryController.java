package com.smartosc.training.controller;

import com.smartosc.training.dto.request.CategoryRequest;
import com.smartosc.training.dto.response.CategoryResponse;
import com.smartosc.training.dto.response.ProductResponse;
import com.smartosc.training.model.Category;
import com.smartosc.training.service.CategoryService;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.Status;
import com.smartosc.training.util.page.SearchCriteria;
import com.smartosc.training.util.page.SpecificationAll;
import com.smartosc.training.util.response.APIResponseDataPagination;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.EcommerceAPIResponsePagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * fres-parent
 *
 * @author Administrator
 * @created_at 09/01/2020 - 12:06 PM
 * @created_by Administrator
 * @since 09/01/2020
 */

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public ResponseEntity<Object> getAll() {
        List<Category> categories = categoryService.getAllCategories();
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(
                        HttpStatus.OK.value(), MessageConstants.SUCCESS,
                        categories.stream().map(x -> new CategoryResponse(x, true))), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getCategory(@PathVariable(MessageConstants.PASS_ID) @Valid @NumberFormat(style = NumberFormat.Style.NUMBER) Long id) {
        Category category = categoryService.getCategory(id);
        if (category == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.CATEGORY_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS,
                        new CategoryResponse(category, true)), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Object> addCategory(@RequestBody CategoryRequest categoryRequest) {
        Category category = new Category();
        Category current = categoryService.addCategoryOrUpdate(category, categoryRequest);
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.CREATED.value(), MessageConstants.CREATE_SUCCESS,
                        new CategoryResponse(current, true)), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@Valid @RequestBody CategoryRequest categoryRequest,
                                         @PathVariable(MessageConstants.PASS_ID) Long id) {
        Category category = categoryService.getCategory(id);
        if (category == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.PRODUCT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        }

        Category current = categoryService.addCategoryOrUpdate(category, categoryRequest);
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, new CategoryResponse(current, true)), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable(MessageConstants.PASS_ID) Long id) {
        Category category = categoryService.getCategory(id);
        if (category == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(
                    HttpStatus.NOT_FOUND.value(), MessageConstants.PRODUCT_NOTFOUND), HttpStatus.NOT_FOUND);
        }
        categoryService.deleteCategory(category);
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Object> getListPage(
            @RequestParam(value = MessageConstants.PASS_SEARCH, required = false) String search,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_PAGE, required = false) int page,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_LIMIT, required = false) int limit) {

        Specification specification = Specification.where(null);
        if (search != null && search.length() > 0) {
            specification =
                    new SpecificationAll(new SearchCriteria(MessageConstants.PASS_NAME, MessageConstants.PASS_OPERATION, search))
                            .or(specification);
        }
        specification = new SpecificationAll(new SearchCriteria(MessageConstants.PASS_CREATE, MessageConstants.PASS_ORDER_BY, MessageConstants.PASS_DES))
                .and(specification);

        specification = new SpecificationAll(new SearchCriteria(MessageConstants.PASS_STATUS, MessageConstants.PASS_OPERATION, Status.ACTIVE.getValue()))
                .and(specification);

        Page<Category> categoryPage = categoryService.categoriesWithPaginate(specification, page, limit);
        return new ResponseEntity<>(new EcommerceAPIResponsePagination<>(
                HttpStatus.OK.value(), MessageConstants.SUCCESS,
                categoryPage.stream().map(x -> new CategoryResponse(x, true)).collect(Collectors.toList()),
                new APIResponseDataPagination(page, limit, categoryPage.getNumber(), categoryPage.getTotalPages(), categoryPage.getTotalElements())),
                HttpStatus.OK);
    }
}