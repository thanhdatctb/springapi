package com.smartosc.training.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;

/**
 * fres-parent
 *
 * @author DuongTrong
 * @created_at 10/01/2020 - 1:52 PM
 * @created_by DuongTrong
 * @since 10/01/2020
 */
public class DateTimeFormat {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    public static String formatDateFromLong(Instant instant) {
        long time = instant.toEpochMilli();
        if (time == 0) {
            return "";
        }
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTimeInMillis(time);
        return simpleDateFormat.format(tempCalendar.getTime());
    }
}
