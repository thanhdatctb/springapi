package com.smartosc.training.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginRequest {

    @NotNull
    @JsonProperty("account")
    private String account;

    @NotNull
    @JsonProperty("password")
    private String password;

    private LoginRequest() {

    }

    public LoginRequest(String account, String password) {
        super();
        this.account = account;
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginRequest [account=" + account + ", password=" + password + "]";
    }

}
