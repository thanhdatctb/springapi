package com.smartosc.training.controller;

import com.smartosc.training.dto.request.ProductRequest;
import com.smartosc.training.dto.response.OrderResponse;
import com.smartosc.training.dto.response.ProductResponse;
import com.smartosc.training.model.Product;
import com.smartosc.training.service.ProductService;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.Status;
import com.smartosc.training.util.page.SearchCriteria;
import com.smartosc.training.util.page.SpecificationAll;
import com.smartosc.training.util.response.APIResponseDataPagination;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.EcommerceAPIResponsePagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<Object> getAll() {
        List<Product> products = productService.getAllProduct();
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS,
                        products.stream().map(x -> new ProductResponse(x, true))), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getProduct(@PathVariable(MessageConstants.PASS_ID) @Valid @NumberFormat(style = NumberFormat.Style.NUMBER) Long id) {
        Product product = productService.getProduct(id);
        if (product == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.PRODUCT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, new ProductResponse(product, true)),
                HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Object> addProduct(@RequestBody ProductRequest productRequest) {
        Product product = new Product();
        Product current = productService.addProductOrUpdate(product, productRequest);
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.CREATED.value(), MessageConstants.CREATE_SUCCESS,
                        new ProductResponse(current, true)), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateProduct(@Valid @RequestBody ProductRequest productRequest,
                                                @PathVariable(MessageConstants.PASS_ID) Long id) {
        Product product = productService.getProduct(id);
        if (product == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.PRODUCT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        }

        Product current = productService.addProductOrUpdate(product, productRequest);
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, new ProductResponse(current, true)), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable(MessageConstants.PASS_ID) Long id) {
        Product product = productService.getProduct(id);
        if (product == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(
                    HttpStatus.NOT_FOUND.value(), MessageConstants.PRODUCT_NOTFOUND), HttpStatus.NOT_FOUND);
        }
        productService.deleteProduct(product);
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<Object> getListPage(
            @RequestParam(value = MessageConstants.PASS_SEARCH, required = false) String search,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_PAGE, required = false) int page,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_LIMIT, required = false) int limit) {

        Specification specification = Specification.where(null);
        if (search != null && search.length() > 0) {
            specification = new SpecificationAll(new SearchCriteria(MessageConstants.PASS_NAME, MessageConstants.PASS_OPERATION, search))
                    .or(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_DESCRIPTION, MessageConstants.PASS_OPERATION, search))
                            .or(specification));
        }
        specification = new SpecificationAll(new SearchCriteria(MessageConstants.PASS_CREATE, MessageConstants.PASS_ORDER_BY, MessageConstants.PASS_DES))
                .and(specification);

        specification = new SpecificationAll(new SearchCriteria(MessageConstants.PASS_STATUS, MessageConstants.PASS_OPERATION, Status.ACTIVE.getValue()))
                .and(specification);

        Page<Product> productPage = productService.productsWithPaginate(specification, page, limit);
        return new ResponseEntity<>(new EcommerceAPIResponsePagination<>(
                HttpStatus.OK.value(), MessageConstants.SUCCESS,
                productPage.stream().map(x -> new ProductResponse(x, true)).collect(Collectors.toList()),
                new APIResponseDataPagination(page, limit, productPage.getNumber(), productPage.getTotalPages(), productPage.getTotalElements())),
                HttpStatus.OK);
    }

    @GetMapping("/count")
    public ResponseEntity<?> getCountProduct() {
        Integer list = productService.findCount();
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, list), HttpStatus.OK);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<?> getListByCategory(
            @RequestParam(value = MessageConstants.PASS_SEARCH, required = false) String search,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_PAGE, required = false) int page,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_LIMIT, required = false) int limit,
            @PathVariable(MessageConstants.PASS_ID) Long id) {
        List<Product> list = productService.findAllByCategory(id);
        Specification specification = Specification.where(null);
        if (search != null && search.length() > 0) {
            specification = specification
                    .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_NAME, MessageConstants.PASS_OPERATION, search)))
                    .or(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_DESCRIPTION, MessageConstants.PASS_OPERATION, search)));
        }

        Long[] foodIds = list.stream().map(Product::getId).toArray(Long[]::new);
        if (foodIds.length == 0) {
            return new ResponseEntity<>(new EcommerceAPIResponsePagination<>(
                    HttpStatus.OK.value(), MessageConstants.SUCCESS, new Long[]{},
                    new APIResponseDataPagination(page, limit, 0, 0, 0)), HttpStatus.OK);
        }

        specification = specification
                .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_ID, "in", foodIds)));

        specification = specification
                .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_CREATE, MessageConstants.PASS_ORDER_BY, MessageConstants.PASS_DES)));

        specification = specification
                .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_STATUS, MessageConstants.PASS_OPERATION, Status.ACTIVE.getValue())));

        Page<Product> products = productService.productsWithPaginate(specification, page, limit);
        return new ResponseEntity<>(new EcommerceAPIResponsePagination<>(
                HttpStatus.OK.value(), MessageConstants.SUCCESS, products.stream()
                .map(x -> new ProductResponse(x, true))
                .collect(Collectors.toList()),
                new APIResponseDataPagination(page, limit, products.getNumber(), products.getTotalPages(), products.getTotalElements())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/suggest")
    public ResponseEntity<?> getListSuggestByFoodId(@Valid @PathVariable("id") Long productId) {
        List<Product> products = productService.suggestByProductId(productId);
        if (products == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.PRODUCT_NOTFOUND), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(
                        HttpStatus.OK.value(),
                        MessageConstants.SUCCESS,
                        products.stream()
                                .map(x -> new ProductResponse(x, true))
                                .collect(Collectors.toList())),
                HttpStatus.OK);
    }

}
