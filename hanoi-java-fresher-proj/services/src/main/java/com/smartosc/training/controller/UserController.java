package com.smartosc.training.controller;

import com.smartosc.training.dto.request.LoginRequest;
import com.smartosc.training.dto.request.UserRequest;
import com.smartosc.training.model.PasswordChange;
import com.smartosc.training.model.User;
import com.smartosc.training.security.JwtTokenProvider;
import com.smartosc.training.service.UserService;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.RoleName;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.JwtAuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping("/register")
    public ResponseEntity<Object> create(@Valid @RequestBody UserRequest userRequest) {
        if (userService.existsByEmail(userRequest.getEmail())) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.BAD_REQUEST.value(), MessageConstants.EMAIL_EXIST),
                    HttpStatus.BAD_REQUEST);
        }

        if (userService.existsByPhone(userRequest.getPhone())) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.BAD_REQUEST.value(), MessageConstants.PHONE_EXIST),
                    HttpStatus.BAD_REQUEST);
        }

        if (userService.existsByUsername(userRequest.getUsername())) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.BAD_REQUEST.value(), MessageConstants.USER_EXIST),
                    HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        User current = userService.registerAndMerge(user, userRequest);
        return new ResponseEntity<>(new EcommerceAPIResponse<>(
                HttpStatus.CREATED.value(), MessageConstants.CREATE_SUCCESS, current), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@Valid @RequestBody LoginRequest loginRequest) {
        Optional<User> user = userService.findByUsernameOrPhone(loginRequest.getAccount(), loginRequest.getAccount());
        if (user.isPresent() && userService.checkRoleByUser(user.get(), RoleName.ROLE_USER)) {
            User current = user.get();
            if (!passwordEncoder.matches(loginRequest.getPassword(), current.getPassword())) {
                return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.UNAUTHORIZED.value(), MessageConstants.PASSWORD_INCORRECT),
                        HttpStatus.UNAUTHORIZED);
            }
        } else {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.UNAUTHORIZED.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.UNAUTHORIZED);
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getAccount(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);

        return new ResponseEntity<>(new JwtAuthenticationResponse(HttpStatus.OK.value(), MessageConstants.SUCCESS, jwt), HttpStatus.OK);
    }

    @PutMapping("/password/change")
    public ResponseEntity<Object> securityChangePassword(@RequestBody @Validated PasswordChange passwordChange) {
        User user = userService.getUserAuth();
        if (user == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.UNAUTHORIZED.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.UNAUTHORIZED);
        } else {
            if (!userService.checkUserPassword(passwordChange.getOldPassword(), user)) {
                return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.UNAUTHORIZED.value(), MessageConstants.PASSWORD_MATCHES),
                        HttpStatus.UNAUTHORIZED);
            } else {
                if (!userService.updatePassword(passwordChange.getPassword(), user)) {
                    return new ResponseEntity<>(
                            new EcommerceAPIResponse<>(HttpStatus.UNAUTHORIZED.value(), MessageConstants.PASSWORD_UPDATE_FALSE),
                            HttpStatus.UNAUTHORIZED);
                } else {
                    return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.PASSWORD_UPDATE_SUCCESS),
                            HttpStatus.OK);
                }
            }
        }
    }
}
