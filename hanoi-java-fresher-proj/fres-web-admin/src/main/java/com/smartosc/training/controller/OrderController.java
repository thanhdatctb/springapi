package com.smartosc.training.controller;

import com.smartosc.training.dto.response.OrderResponse;
import com.smartosc.training.exception.APIException;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.service.RestTemplateService;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private RestTemplateService restTemplateService;

    private HttpHeaders httpHeaders = new HttpHeaders();

    @Value("${services.path}" + "/order/list")
    private String API_URL;

    @Value("${auth}")
    private String auth;

    @GetMapping
    public String getListOrder(Model model, HttpServletRequest request) {
        try {
            httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
            ParameterizedTypeReference<EcommerceAPIResponse<List<OrderResponse>>> type = new ParameterizedTypeReference<EcommerceAPIResponse<List<OrderResponse>>>() {
            };
            List<OrderResponse> response = restTemplateService.getObjects(API_URL, HttpMethod.GET, httpHeaders, null, type);
            model.addAttribute("orders", response);
        } catch (APIException apiE) {
            model.addAttribute("error", apiE.getMessage());
            return apiE.getMessage();
        }

        return "order/data";
    }
}
