package com.smartosc.training.controller;

import com.smartosc.training.dto.request.OrderRequest;
import com.smartosc.training.dto.response.OrderResponse;
import com.smartosc.training.exception.ErrorDetails;
import com.smartosc.training.model.Order;
import com.smartosc.training.model.User;
import com.smartosc.training.service.OrderService;
import com.smartosc.training.service.UserService;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.OrderStatus;
import com.smartosc.training.util.enumm.RoleName;
import com.smartosc.training.util.page.SearchCriteria;
import com.smartosc.training.util.page.SpecificationAll;
import com.smartosc.training.util.response.APIResponseDataPagination;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.EcommerceAPIResponsePagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<?> getOrderInCart() {
        User user = userService.getUserAuth();
        if (null == user) {
            return new ResponseEntity<>(
                    new ErrorDetails(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        } else {
            List<Order> orderList = orderService.getByStatus(user, OrderStatus.IN_CART.getValue());
            Order order;
            if (orderList.size() == 0) {
                order = orderService.saveOrderByUser(user, new OrderRequest());
            } else {
                order = orderList.get(0);
            }
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(),
                    MessageConstants.SUCCESS, new OrderResponse(order, true)), HttpStatus.OK);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll() {
        User user = userService.getUserAuth();
        if (user == null) {
            return new ResponseEntity<>(
                    new ErrorDetails(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        } else {
            List<Order> orders = orderService.getAllOrder();
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, orders.stream().map(x -> new OrderResponse(x, true))), HttpStatus.OK);
        }
    }

    @GetMapping("/count")
    public ResponseEntity<?> getCountOrder() {
        Integer list = orderService.findCount();
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, list), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createOrder(@Valid @RequestBody OrderRequest orderRequest) {
        User user = userService.getUserAuth();
        if (user == null) {
            return new ResponseEntity<>(
                    new ErrorDetails(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        } else {
            Order order = orderService.saveOrderByUser(user, orderRequest);
            if (order == null) {
                return new ResponseEntity<>(
                        new ErrorDetails(HttpStatus.BAD_REQUEST.value(), MessageConstants.ITEM_NOT_MATCH),
                        HttpStatus.BAD_REQUEST);
            }
            OrderResponse orderResponse = new OrderResponse(order, true);
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.CREATED.value(),
                    MessageConstants.SUCCESS_ORDER, orderResponse), HttpStatus.CREATED);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateOrderDetail(@PathVariable Long id, @Valid @RequestBody OrderRequest orderRequest) {
        User user = userService.getUserAuth();
        if (user == null) {
            return new ResponseEntity<>(
                    new ErrorDetails(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        } else {
            Order order = orderService.updateOrder(id, orderRequest);
            if (order == null) {
                return new ResponseEntity<>(
                        new ErrorDetails(HttpStatus.BAD_REQUEST.value(), MessageConstants.ITEM_NOT_MATCH),
                        HttpStatus.BAD_REQUEST);
            }
            OrderResponse orderResponse = new OrderResponse(order, true);
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.CREATED.value(),
                    MessageConstants.SUCCESS_ORDER, orderResponse), HttpStatus.CREATED);
        }

    }

    @PutMapping("/status/{id}")
    public ResponseEntity<?> updateOrderStatus(@PathVariable("id") Long id, @RequestParam Integer status) {
        User user = userService.getUserAuth();
        if (user == null) {
            return new ResponseEntity<>(
                    new ErrorDetails(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        } else {
            Order order = orderService.updateStatusOrder(id, status);
            if (order == null) {
                return new ResponseEntity<>(
                        new ErrorDetails(HttpStatus.BAD_REQUEST.value(), MessageConstants.ITEM_NOT_MATCH),
                        HttpStatus.BAD_REQUEST);
            }
            OrderResponse orderResponse = new OrderResponse(order, true);
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.CREATED.value(),
                    MessageConstants.SUCCESS_ORDER, orderResponse), HttpStatus.CREATED);
        }

    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getOrderDetail(@PathVariable("id") Long id) {
        User user = userService.getUserAuth();
        if (user == null) {
            return new ResponseEntity<>(
                    new ErrorDetails(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        } else {
            OrderResponse orderResponse = orderService.getById(id);
            if (orderResponse == null) {
                return new ResponseEntity<>(
                        new ErrorDetails(HttpStatus.NOT_FOUND.value(), MessageConstants.ORDER_NOTFOUND),
                        HttpStatus.NOT_FOUND);
            }
            if (!orderResponse.getUserId().equals(user.getId())) {
                return new ResponseEntity<>(
                        new ErrorDetails(HttpStatus.BAD_REQUEST.value(), MessageConstants.ITEM_NOT_ACCEPT),
                        HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(
                    new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, orderResponse),
                    HttpStatus.OK);
        }
    }

    @GetMapping("/list")
    public ResponseEntity<?> getListByUser(
            @RequestParam(value = MessageConstants.PASS_SEARCH, required = false) String search,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_PAGE, required = false) int page,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_LIMIT, required = false) int limit,
            @RequestParam(value = MessageConstants.PASS_STATUS, required = false) Integer status
    ) {
        User user = userService.getUserAuth();
        if (user == null) {
            return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND), HttpStatus.NOT_FOUND);
        }

        Specification specification = Specification.where(null);
        if (search != null && search.length() > 0) {
            specification = specification
                    .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_ID, MessageConstants.PASS_OPERATION, search)))
                    .or(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_TITLE, MessageConstants.PASS_OPERATION, search)));
        }
        specification = specification
                .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_CREATE, MessageConstants.PASS_ORDER_BY, MessageConstants.PASS_DES)));

        if (userService.checkRoleByUser(user, RoleName.ROLE_USER)) {
            List<Order> orders = orderService.getAllByUser(user);

            Long[] orderIds = orders.stream().map(Order::getId).toArray(Long[]::new);
            if (orderIds.length == 0) {
                return new ResponseEntity<>(new EcommerceAPIResponsePagination<>(
                        HttpStatus.OK.value(), MessageConstants.SUCCESS, new Long[]{},
                        new APIResponseDataPagination(page, limit, 0, 0, 0)), HttpStatus.OK);
            }

            specification = specification
                    .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_ID, MessageConstants.PASS_IN, orderIds)));
        }
        if (status != null) {
            specification = specification
                    .and(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_STATUS, MessageConstants.PASS_OPERATION, status)));
        }

        Page<Order> orderPage = orderService.getAllByUserWithPaginate(specification, page, limit);
        return new ResponseEntity<>(new EcommerceAPIResponsePagination<>(
                HttpStatus.OK.value(), MessageConstants.SUCCESS, orderPage.stream()
                .map(x -> new OrderResponse(x, true))
                .collect(Collectors.toList()),
                new APIResponseDataPagination(page, limit, orderPage.getNumber(), orderPage.getTotalPages(), orderPage.getTotalElements())), HttpStatus.OK);

    }

}
