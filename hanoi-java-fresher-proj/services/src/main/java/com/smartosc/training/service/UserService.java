package com.smartosc.training.service;

import com.smartosc.training.dto.request.CurrentUserRequest;
import com.smartosc.training.dto.request.UserRequest;
import com.smartosc.training.model.User;
import com.smartosc.training.util.enumm.RoleName;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User registerAndMerge(User user, UserRequest userRequest);

    User registerAdmin(User user, UserRequest userRequest);

    Optional<User> findByUsernameOrPhone(String email, String phone);

    boolean checkRoleByUser(User user, RoleName roleName);

    boolean existsByUsername(String username);

    Integer findCount();

    boolean existsByPhone(String phone);

    boolean existsByEmail(String email);

    List<User> findAll();

    User getUserAuth();

    User findById(Long id);

    User update(User user, CurrentUserRequest userRequest);

    void delete(User user);

    boolean checkUserPassword(String oldPassword, User user);

    boolean updatePassword(String password, User user);

    Page<User> usersWithPaginate(Specification specification, int page, int limit);

    List<String> getUsernameList();
}
