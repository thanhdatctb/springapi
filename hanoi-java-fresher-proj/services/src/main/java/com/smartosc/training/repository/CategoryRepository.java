package com.smartosc.training.repository;

import com.smartosc.training.model.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * fres-parent
 *
 * @author Administrator
 * @created_at 09/01/2020 - 12:02 PM
 * @created_by Administrator
 * @since 09/01/2020
 */

@Repository
public interface CategoryRepository extends CustomRepository<Category, Long> {

    @Query("SELECT c from Category c WHERE c.status = 1 ORDER BY c.createdAt DESC ")
    List<Category> findAllByStatusOrderByCreatedAtDesc(Integer status);

    Category findByIdAndStatus(Long id, Integer status);

    List<Category> findAllByIdIn(Collection<Long> Ids);
}
