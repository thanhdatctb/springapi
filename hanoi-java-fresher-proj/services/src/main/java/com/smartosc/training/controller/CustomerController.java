package com.smartosc.training.controller;

import com.smartosc.training.dto.request.CurrentUserRequest;
import com.smartosc.training.dto.response.UserResponse;
import com.smartosc.training.model.User;
import com.smartosc.training.service.UserService;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.Status;
import com.smartosc.training.util.page.SearchCriteria;
import com.smartosc.training.util.page.SpecificationAll;
import com.smartosc.training.util.response.APIResponseDataPagination;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.EcommerceAPIResponsePagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<?> getList() {
        List<User> userList = userService.findAll();
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS,
                        userList.stream().map(x -> new UserResponse(x, true)).collect(Collectors.toList())),
                HttpStatus.OK);
    }

    @GetMapping("/profile")
    public ResponseEntity<?> getUserProfile() {
        User user = userService.getUserAuth();
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, new UserResponse(user, true)), HttpStatus.OK);
    }

    @GetMapping("/usernames")
    public ResponseEntity<?> getListUsername() {
        List<String> usernameList = userService.getUsernameList();
        return new ResponseEntity<>(
                new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, usernameList.stream()
                        .map(x -> new String(Base64.getEncoder().encode(x.getBytes()))).collect(Collectors.toList())),
                HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomer(
            @PathVariable(MessageConstants.PASS_ID) @Valid @NumberFormat(style = NumberFormat.Style.NUMBER) Long id) {
        User user = userService.findById(id);
        if (user == null) {
            return new ResponseEntity<>(
                    new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS,
                new UserResponse(user, true)), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody CurrentUserRequest userRequest, @PathVariable("id") Long id) {
        User user = userService.findById(id);
        if (user == null) {
            return new ResponseEntity<>(
                    new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        }
        User current = userService.update(user, userRequest);
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS,
                new UserResponse(current, true)), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable(MessageConstants.PASS_ID) Long id) {
        User user = userService.findById(id);
        if (user == null) {
            return new ResponseEntity<>(
                    new EcommerceAPIResponse<>(HttpStatus.NOT_FOUND.value(), MessageConstants.ACCOUNT_NOTFOUND),
                    HttpStatus.NOT_FOUND);
        }

        userService.delete(user);
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS),
                HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<?> getListPage(
            @RequestParam(value = MessageConstants.PASS_SEARCH, required = false) String search,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_PAGE, required = false) int page,
            @RequestParam(defaultValue = MessageConstants.PASS_VALUE_DEFAULT_LIMIT, required = false) int limit) {

        Specification specification = Specification.where(null);
        if (search != null && search.length() > 0) {
            specification = new SpecificationAll(
                    new SearchCriteria(MessageConstants.PASS_PHONE, MessageConstants.PASS_OPERATION, search))
                    .or(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_EMAIL,
                            MessageConstants.PASS_OPERATION, search))
                            .or(new SpecificationAll(new SearchCriteria(MessageConstants.PASS_USER,
                                    MessageConstants.PASS_OPERATION, search)).or(specification)));
        }
        specification = new SpecificationAll(new SearchCriteria(MessageConstants.PASS_CREATE,
                MessageConstants.PASS_ORDER_BY, MessageConstants.PASS_DES)).and(specification);

        specification = new SpecificationAll(new SearchCriteria(MessageConstants.PASS_STATUS,
                MessageConstants.PASS_OPERATION, Status.ACTIVE.getValue())).and(specification);

        Page<User> userPage = userService.usersWithPaginate(specification, page, limit);
        return new ResponseEntity<>(new EcommerceAPIResponsePagination<>(HttpStatus.OK.value(),
                MessageConstants.SUCCESS,
                userPage.stream().map(x -> new UserResponse(x, true)).collect(Collectors.toList()),
                new APIResponseDataPagination(page, limit, userPage.getNumber(), userPage.getTotalPages(), userPage.getTotalElements())),
                HttpStatus.OK);
    }

    @GetMapping("/count")
    public ResponseEntity<?> getCount() {
        Integer longs = userService.findCount();
        return new ResponseEntity<>(new EcommerceAPIResponse<>(HttpStatus.OK.value(), MessageConstants.SUCCESS, longs), HttpStatus.OK);
    }

}
