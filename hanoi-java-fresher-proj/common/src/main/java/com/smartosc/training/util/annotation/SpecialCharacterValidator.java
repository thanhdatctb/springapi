package com.smartosc.training.util.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * fres-parent
 *
 * @author DuongTrong
 * @created_at 13/01/2020 - 11:19 AM
 * @created_by DuongTrong
 * @since 13/01/2020
 */

public class SpecialCharacterValidator implements ConstraintValidator<NotContainSpecialCharacter, String> {
    @Override
    public void initialize(NotContainSpecialCharacter constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !isSpecialCharacter(value);
    }

    private static boolean isSpecialCharacter(String value) {
        Pattern special = Pattern.compile("[!@#$%&*()+=|<>?{}\\[\\]~-]");
        Matcher hasSpecial = special.matcher(value);
        return hasSpecial.find();
    }
}