package com.smartosc.training.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class OrderDetailKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "order_id")
    public Long order_id;

    @Column(name = "product_id")
    public Long product_id;
}
