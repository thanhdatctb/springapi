package com.smartosc.training.util.page;

import com.smartosc.training.model.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * hanoi-java-fresher-proj
 *
 * @author DuongTrong
 * @created_at 09/01/2020 - 2:37 PM
 * @created_by DuongTrong
 * @since 09/01/2020
 */

public class SpecificationAll implements org.springframework.data.jpa.domain.Specification<User> {

    private SearchCriteria criteria;

    public SpecificationAll(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
        if (criteria.getOperation().equalsIgnoreCase("orderBy")) {
            if (criteria.getValue().toString().equals("desc")) {
                criteriaQuery.orderBy(builder.desc(root.get(criteria.getKey())));
            }
            if (criteria.getValue().toString().equals("asc")) {
                criteriaQuery.orderBy(builder.asc(root.get(criteria.getKey())));
            }
        }

        if (criteria.getOperation().equalsIgnoreCase(">=")) {
            return builder.greaterThanOrEqualTo(root.get(criteria.getKey()), criteria.getValue().toString());
        } else if (criteria.getOperation().equalsIgnoreCase("<=")) {
            return builder.lessThanOrEqualTo(root.get(criteria.getKey()), criteria.getValue().toString());
        } else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return builder.like(
                        root.get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return builder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        } else if (criteria.getOperation().equalsIgnoreCase("in")) {
            CriteriaBuilder.In<Long> inClause = builder.in(root.get(criteria.getKey()));
            Long[] ids = (Long[]) criteria.getValue();
            for (Long id : ids) {
                inClause.value(id);
            }
            return inClause;
        }
        return null;
    }
}