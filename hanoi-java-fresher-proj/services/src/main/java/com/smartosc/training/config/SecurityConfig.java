package com.smartosc.training.config;

import com.smartosc.training.security.CustomUserDetailsService;
import com.smartosc.training.security.JwtAuthenticationEntryPoint;
import com.smartosc.training.security.JwtAuthenticationFilter;
import com.smartosc.training.util.ApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/",
                        "/favicon.ico",
                        "/**/*.png",
                        "/**/*.gif",
                        "/**/*.svg",
                        "/**/*.jpg",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js")
                .permitAll()
                .antMatchers(ApiConstants.GET_PUBLIC_CLIENT)
                .permitAll()
                .antMatchers(HttpMethod.POST, ApiConstants.PRIVATE_URL)
                .hasAnyAuthority(ApiConstants.ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, ApiConstants.PRIVATE_URL)
                .hasAnyAuthority(ApiConstants.ROLE_ADMIN)
                .antMatchers(HttpMethod.DELETE, ApiConstants.PRIVATE_URL)
                .hasAnyAuthority(ApiConstants.ROLE_ADMIN)
                .antMatchers(HttpMethod.POST, ApiConstants.PRIVATE_ADMIN)
                .hasAnyAuthority(ApiConstants.ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, ApiConstants.PRIVATE_ADMIN)
                .hasAnyAuthority(ApiConstants.ROLE_ADMIN)
                .antMatchers(HttpMethod.GET, ApiConstants.GET_PUBLIC_CLIENT)
                .permitAll()
                .anyRequest()
                .authenticated();

        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
