package com.smartosc.training.util.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class APIResponseDataPagination implements Serializable {

    private int page;
    private int limit;
    private int number;
    private int totalPages;
    private long totalItems;

    public APIResponseDataPagination(int page, int limit, int number, int totalPages, long totalItems) {
        this.page = page;
        this.limit = limit;
        this.number = number;
        if (limit != 0) {
            this.totalPages = (totalItems % limit == 0) ? (int) (totalItems / limit) : ((int) (totalItems / limit) + 1);
        } else
            this.totalPages = 0;
        this.totalItems = totalItems;
    }

    public APIResponseDataPagination() {

    }
}
