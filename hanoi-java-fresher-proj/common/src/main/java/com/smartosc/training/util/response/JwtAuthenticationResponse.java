package com.smartosc.training.util.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtAuthenticationResponse {
    private int status;
    private String message;
    private String accessToken;

    private JwtAuthenticationResponse() {

    }

    public JwtAuthenticationResponse(int status, String message, String accessToken) {
        this();
        this.status = status;
        this.message = message;
        String tokenType = "Bearer";
        this.accessToken = tokenType + " " + accessToken;
    }
}
