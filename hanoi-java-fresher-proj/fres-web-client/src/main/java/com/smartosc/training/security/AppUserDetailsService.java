package com.smartosc.training.security;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.smartosc.training.dto.request.LoginRequest;
import com.smartosc.training.springrest.RestTemplateService;
import com.smartosc.training.util.response.JwtAuthenticationResponse;

public class AppUserDetailsService implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HttpServletRequest httpRequest;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private RestTemplateService restTemplateService;

    @Value("${services.apiPath}")
    private String apiPath;

    @Override
    public UserDetails loadUserByUsername(final String username) {
        String password = httpRequest.getParameter("password");
        logger.info("loadUserByUsername username= {}", username);
        String url = apiPath + "/login";
        LoginRequest request = new LoginRequest(username, password);
        JwtAuthenticationResponse response = restTemplateService.getJwtToken(url, request);
        String token = response.getAccessToken();
        AppUserDetails appUserDetails = new AppUserDetails();
        appUserDetails.setJwtToken(token);
        appUserDetails.setPassword(encoder.encode(password));
        appUserDetails.setUsername(username);
        return appUserDetails;
    }

}