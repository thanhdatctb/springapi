package com.smartosc.training.services;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.smartosc.training.dto.request.UserRequest;
import com.smartosc.training.dto.response.UserResponse;

@Service
public class UserRegistrationService {
    private HashMap<String, UserRequest> registrationRequests;
    private List<UserResponse> existingUsers;
    public String[] usernameList;
    private List<String> emailList;
    private List<String> phoneList;

    public UserRegistrationService() {
        this.registrationRequests = new HashMap<>();
        this.existingUsers = new ArrayList<>();
    }

    public String enqueueRequest(UserRequest request) {
        String token = new String(Base64.getEncoder().encode(request.getUsername().getBytes()));
        registrationRequests.put(token, request);
        return token;
    }

    public UserRequest dequeueRequest(String token) {
        UserRequest confirmedRequest = registrationRequests.get(token);
        registrationRequests.remove(token);
        return confirmedRequest;
    }

    public List<UserResponse> getExistingUsers() {
        return existingUsers;
    }

    public void setExistingUsers(List<UserResponse> existingUsers) {
        this.existingUsers = existingUsers;
        this.emailList = existingUsers.stream().map(UserResponse::getEmail).collect(Collectors.toList());
        this.phoneList = existingUsers.stream().map(UserResponse::getPhone).collect(Collectors.toList());
    }

    public void validateInput(String email, String phone) {
        if (emailList.contains(email) || phoneList.contains(phone)) {
            throw new DuplicateKeyException("Entry already exist");
        }
    }

}
