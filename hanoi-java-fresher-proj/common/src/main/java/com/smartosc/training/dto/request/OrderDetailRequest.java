package com.smartosc.training.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class OrderDetailRequest implements Serializable {
    private Long productId;
    private int quantity;

    public OrderDetailRequest() {

    }

    public OrderDetailRequest(Long id, int quantity) {
        this.productId = id;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "OrderDetailRequest [productId=" + productId + ", quantity=" + quantity + "]";
    }

}
