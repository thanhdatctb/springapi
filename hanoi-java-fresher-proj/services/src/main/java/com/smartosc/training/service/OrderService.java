package com.smartosc.training.service;

import com.smartosc.training.dto.request.OrderRequest;
import com.smartosc.training.dto.response.OrderResponse;
import com.smartosc.training.model.Order;
import com.smartosc.training.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface OrderService {

    List<Order> getAllOrder();

    List<Order> getAllByUser(User user);

    Page<Order> getAllByUserWithPaginate(Specification specification, int page, int limit);

    Order saveOrderByUser(User user, OrderRequest orderRequest);

    Order updateStatusOrder(Long id, Integer status);

    OrderResponse getById(Long id);

    Integer findCount();

    List<Order> getAllByCreatedAtBetween(String from, String to);

    List<Order> getByStatus(User user, Integer status);

    Order updateOrder(Long id, OrderRequest orderRequest);
}
