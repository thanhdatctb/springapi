package com.smartosc.training.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
public class PasswordChange implements Serializable {

    @NotNull
    @JsonProperty("old_password")
    @Column(name = "old_password")
    @NotEmpty(message = "Password must be entered.")
    private String oldPassword;

    @NotNull
    @Column(name = "password")
    @JsonProperty("password")
    @NotEmpty(message = "Password must be entered.")
    @Size(min = 6, max = 50, message = "Enter a password between 6 and 50 characters.")
    private String password;

    @NotNull
    @JsonProperty("confirm_password")
    @Column(name = "confirm_password")
    @NotEmpty(message = "Confirm password")
    @Size(min = 6, max = 50, message = "Enter a password between 6 and 50 characters.")
    private String confirmPassword;
}
