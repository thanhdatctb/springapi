package com.smartosc.training.util.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * hanoi-java-fresher-proj
 *
 * @author DuongTrong
 * @created_at 09/01/2020 - 2:49 PM
 * @created_by DuongTrong
 * @since 09/01/2020
 */

@Documented
@Constraint(validatedBy = PasswordsEqualConstraintValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
public @interface PasswordsEqualConstraint {

    String message() default "{password.not.match}";

    String field() default "{password.not.match}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

