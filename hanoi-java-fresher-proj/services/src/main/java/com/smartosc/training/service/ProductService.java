package com.smartosc.training.service;

import com.smartosc.training.dto.request.ProductRequest;
import com.smartosc.training.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

    Product getProduct(Long id);

    Product addProductOrUpdate(Product product, ProductRequest productRequest);

    void deleteProduct(Product product);

    Page<Product> productsWithPaginate(Specification specification, int page, int limit);

    List<Product> findAllByCategory(Long categoryId);

    List<Product> suggestByProductId(Long productId);

    Integer findCount();

}
