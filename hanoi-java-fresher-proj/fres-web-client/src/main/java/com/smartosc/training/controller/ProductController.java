package com.smartosc.training.controller;

import java.util.List;

import com.smartosc.training.dto.response.CategoryResponse;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import com.smartosc.training.util.response.EcommerceAPIResponsePagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.smartosc.training.dto.response.ProductResponse;
import com.smartosc.training.exception.APIException;
import com.smartosc.training.springrest.RestTemplateService;

@Controller
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private RestTemplateService restTemplateService;

    @Value("${services.apiPath}")
    private String baseUrl;

    @GetMapping
    public String getProductList(@RequestParam(value = "page", defaultValue = "2", required = false) Integer page,
                                 @RequestParam(value = "limit", defaultValue = "8", required = false) Integer limit,
                                 Model model) {
        String url = null == page ? baseUrl + "/products/list" : baseUrl + "/products/list?page=" + page + "&limit=" + limit;
        try {
            ParameterizedTypeReference<EcommerceAPIResponsePagination<List<ProductResponse>>> type = new ParameterizedTypeReference<EcommerceAPIResponsePagination<List<ProductResponse>>>() {
            };
            EcommerceAPIResponsePagination<List<ProductResponse>> response = restTemplateService.getPaginatedObjects(url, null, type);
            model.addAttribute("products", response.getBody());
            model.addAttribute("pagination", response.getPagination());
            System.out.println(response.getPagination());
        } catch (APIException apiE) {
            model.addAttribute("error", apiE.getMessage());
            return apiE.getMessage();
        }

        return "shop";
    }

    @GetMapping("/{id}")
    public String getProductByCategories(@PathVariable("id") Long id,
                                         @RequestParam(value = "page", defaultValue = "1", required = false) Integer page,
                                         @RequestParam(value = "limit", defaultValue = "8", required = false) Integer limit,
                                         Model model) {
        String url = null == page ? baseUrl + "/products/category/" + id : baseUrl + "/products/category/" + id + "?page=" + page + "&limit=" + limit;
        try {
            ParameterizedTypeReference<EcommerceAPIResponsePagination<List<ProductResponse>>> type = new ParameterizedTypeReference<EcommerceAPIResponsePagination<List<ProductResponse>>>() {
            };
            EcommerceAPIResponsePagination<List<ProductResponse>> response = restTemplateService.getPaginatedObjects(url, null, type);
            model.addAttribute("bodyProduct", response.getBody());
            model.addAttribute("pagination", response.getPagination());
            model.addAttribute("id", id);

            ParameterizedTypeReference<EcommerceAPIResponse<List<CategoryResponse>>> typeReferences =
                    new ParameterizedTypeReference<EcommerceAPIResponse<List<CategoryResponse>>>() {
                    };
            List<CategoryResponse> categoryResponses = restTemplateService.getObjectsss(baseUrl + "/categories",
                    HttpMethod.GET, null, null, typeReferences);
            model.addAttribute("categoryResponses", categoryResponses);
        } catch (APIException e) {
            model.addAttribute("error", e.getMessage());
            return e.getMessage();
        }
        return "shop";
    }

}
