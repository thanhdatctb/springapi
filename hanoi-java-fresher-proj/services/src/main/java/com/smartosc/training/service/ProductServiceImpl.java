package com.smartosc.training.service;

import com.smartosc.training.dto.request.ProductRequest;
import com.smartosc.training.exception.CustomRuntimeException;
import com.smartosc.training.model.Category;
import com.smartosc.training.model.Product;
import com.smartosc.training.repository.ProductRepository;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryService categoryService;

    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllByStatusOrderByCreatedAtDesc(Status.ACTIVE.getValue());
    }

    @Override
    public Product getProduct(Long id) {
        if (CollectionUtils.isEmpty(Collections.singleton(id))) {
            throw new CustomRuntimeException(MessageConstants.NULL_POINTER);
        }
        return productRepository.findByIdAndStatus(id, Status.ACTIVE.getValue());
    }

    @Override
    public Product addProductOrUpdate(Product product, ProductRequest productRequest) {
        if (productRequest.getName() != null) product.setName(productRequest.getName());
        if (productRequest.getImage() != null) product.setImage(productRequest.getImage());
        if (productRequest.getDescription() != null) product.setDescription(productRequest.getDescription());
        if (productRequest.getPrice() != 0.0f) product.setPrice(productRequest.getPrice());
        if (product.getStatus() == null) product.setStatus(Status.ACTIVE.getValue());
        if (productRequest.getCategories() != null && productRequest.getCategories().size() > 0) {
            List<Category> categories = categoryService.findAllByIdIn(productRequest.getCategories());
            Set<Category> categorySet = new HashSet<>(categories);
            product.setCategories(categorySet);
        }
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(Product product) {
        product.setStatus(Status.DEACTIVE.getValue());
        productRepository.save(product);
    }

    @Override
    public Page<Product> productsWithPaginate(Specification specification, int page, int limit) {
        return productRepository.findAll(specification, PageRequest.of(page - 1, limit));
    }

    @Override
    public List<Product> findAllByCategory(Long categoryId) {
        Category category = categoryService.getCategory(categoryId);
        return productRepository.findAllByStatusAndCategories(Status.ACTIVE.getValue(), category);
    }

    @Override
    public List<Product> suggestByProductId(Long productId) {
        Product product = productRepository.findByIdAndStatus(productId, Status.ACTIVE.getValue());
        if (product == null)
            return null;

        List<Category> categories = new ArrayList<>(product.getCategories());
        if (!categories.isEmpty()) {
            categories = categoryService.getAllCategories();
        }
        return productRepository.findAllByStatusAndCategoriesIn(Status.ACTIVE.getValue(), categories);
    }

    @Override
    public Integer findCount() {
        return productRepository.findCount();
    }
}
