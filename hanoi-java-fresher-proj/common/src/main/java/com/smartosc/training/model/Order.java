package com.smartosc.training.model;

import java.util.Set;

import javax.persistence.*;

import com.smartosc.training.util.enumm.OrderStatus;
import com.smartosc.training.util.enumm.Status;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "orders")
public class Order extends DateAudit {

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "totalPrice", columnDefinition = "Decimal(10,2)")
    private float totalPrice;

    @Column(name = "title")
    private String title;

    @Column(name = "address")
    private String address;

    @Column(name = "status")
    private Integer status;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private Set<OrderDetail> orderDetails;

    public Order() {
    }

    public Order(float totalPrice, String title, String address, User user) {
        this.totalPrice = totalPrice;
        this.title = title;
        this.address = address;
        this.user = user;
        this.status = OrderStatus.IN_CART.getValue();
    }

    @Override
    public String toString() {
        return "Order [id=" + id + ", totalPrice=" + totalPrice + ", title=" + title + ", address=" + address
                + ", status=" + status + ", user=" + user + ", orderDetails=" + orderDetails + "]";
    }

}