package com.smartosc.training.util.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * hanoi-java-fresher-proj
 *
 * @author DuongTrong
 * @created_at 09/01/2020 - 1:30 PM
 * @created_by DuongTrong
 * @since 09/01/2020
 */

@Getter
@Setter
public class EcommerceAPIResponse<T> implements Serializable {

    private int status;
    private String message;
    private T body;

    public EcommerceAPIResponse() {
    }

    public EcommerceAPIResponse(int status, String message, T body) {
        this.status = status;
        this.message = message;
        this.body = body;
    }

    public EcommerceAPIResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    public String toString() {
        return "EcommerceAPIResponse [status=" + status + ", message=" + message + ", body=" + body + "]";
    }
}
