package com.smartosc.training.dto.response;

import com.smartosc.training.model.Product;
import com.smartosc.training.util.DateTimeFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * fres-parent
 *
 * @author DuongTrong
 * @created_at 10/01/2020 - 1:45 PM
 * @created_by DuongTrong
 * @since 10/01/2020
 */

@Getter
@Setter
public class ProductResponse implements Serializable {

    private Long id;
    private String name;
    private String image;
    private String description;
    private float price;
    private String createdAt;
    private String updatedAt;
    private Integer status;
    private List<CategoryResponse> categories = new ArrayList<>();

    public ProductResponse() {
    }

    public ProductResponse(Product product, boolean hasCategory) {
        this.id = product.getId();
        this.name = product.getName();
        this.image = product.getImage();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.createdAt = DateTimeFormat.formatDateFromLong(product.getCreatedAt());
        this.updatedAt = DateTimeFormat.formatDateFromLong(product.getUpdatedAt());
        this.status = product.getStatus();
        if (hasCategory)
            this.categories = product.getCategories().stream().map(x -> new CategoryResponse(x, false)).collect(Collectors.toList());
    }
}
