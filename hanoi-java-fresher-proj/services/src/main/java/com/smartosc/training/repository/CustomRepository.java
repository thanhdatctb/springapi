package com.smartosc.training.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * fres-parent
 *
 * @author trangduong
 * @version 1.0
 * @create_by trangduong
 * @created_date 12/01/2020 - 11:33 AM
 * @since 12/01/2020
 */

@NoRepositoryBean
public interface CustomRepository<E, K> extends JpaRepository<E, K>, JpaSpecificationExecutor {
}
