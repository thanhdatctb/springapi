package com.smartosc.training.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.smartosc.training.dto.request.UserRequest;
import com.smartosc.training.dto.response.BaseResponse;
import com.smartosc.training.dto.response.UserResponse;
import com.smartosc.training.exception.APIException;
import com.smartosc.training.model.PasswordChange;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.services.SMTPService;
import com.smartosc.training.services.UserRegistrationService;
import com.smartosc.training.springrest.RestTemplateService;
import com.smartosc.training.util.MailContentBuilder;

@Controller
public class AuthenticationController {

    @Autowired
    private MailContentBuilder mailContentBuilder;

    @Autowired
    private SMTPService smtpService;

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRegistrationService userRegistrationService;

    @Value("${services.apiPath}" + "/auth")
    private String apiAuthPath;

    @Value("${services.apiPath}")
    private String apiPath;

    @Value("${server.baseUrl}")
    private String baseUrl;

    @GetMapping("/register")
    public String registerForm(@ModelAttribute UserRequest userRequest, Model model) {
        if (userRegistrationService.usernameList == null || userRegistrationService.usernameList.length == 0) {
            String url = apiPath + "/customer/usernames";
            BaseResponse<String[]> usernamesResponse = restTemplateService.getObjects(url, null,
                    new HashMap<String, String>());
            userRegistrationService.usernameList = modelMapper.map(usernamesResponse.getBody(), String[].class);
            url = apiPath + "/customer";
            BaseResponse<ArrayList<LinkedHashMap<String, Object>>> userListResponse = restTemplateService.getObjects(url, null,
                    new HashMap<String, String>());
            userRegistrationService.setExistingUsers(userListResponse.getBody().stream()
                    .map(userResponse -> modelMapper.map(userResponse, UserResponse.class))
                    .collect(Collectors.toList()));
        }
        model.addAttribute("usernames", userRegistrationService.usernameList);
        return "registerForm";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute UserRequest request, Model model, BindingResult result) {
        try {
//			validate input
            if (result.hasErrors()) {
                model.addAttribute("request", request);
                return "registerForm";
            }
            userRegistrationService.validateInput(request.getEmail(), request.getPhone());
//			generate confirmation token
            String registerToken = userRegistrationService.enqueueRequest(request);
//			send confirmation email
            String confirmationUrl = baseUrl + "/register/confirmation/" + registerToken;

            String mailContent = mailContentBuilder.buildRegisterConfirmationTemplate(request.getEmail(),
                    confirmationUrl);
            smtpService.sendEmail(new String[]{request.getEmail()}, null, "Confirmation Email", mailContent);
            model.addAttribute("email", request.getEmail());
            return "registerConfirmation";
        } catch (Exception apiE) {
            System.out.println(apiE.getMessage());
            model.addAttribute("error", apiE.getMessage());
            model.addAttribute("usernames", userRegistrationService.usernameList);
            return "registerForm";
        }

    }

    @GetMapping("/register/confirmation/{registerToken:.*}")
    private String registerConfirmation(@PathVariable String registerToken, Model model) {
        String url = apiAuthPath + "/register";
        UserRequest request = userRegistrationService.dequeueRequest(registerToken);
        BaseResponse<UserResponse> response = restTemplateService.createObject(url, null, request);
        model.addAttribute("user", request.getEmail());
        return "registerSuccess";
    }

    @GetMapping("/resetPassword")
    public String resetPasswordForm(Model model) {
        model.addAttribute("formDetail", new PasswordChange());
        return "user-profile";
    }

    @PostMapping("/resetPassword")
    public String resetPassword(HttpServletRequest request, @ModelAttribute PasswordChange formDetail, Model model) {
        String url = apiAuthPath + "/password/change";
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", SecurityUtils.getJWTToken(request));
            BaseResponse<UserResponse> response = restTemplateService.createObject(url, headers, request);
            String result = modelMapper.map(response.getBody(), String.class);
            model.addAttribute("message", result);
            return "user-profile";
        } catch (APIException apiE) {
            model.addAttribute("error", apiE.getMessage());
            return "resetPasswordForm";
        }
    }
}
