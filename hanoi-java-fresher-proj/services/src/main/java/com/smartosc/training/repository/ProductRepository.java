package com.smartosc.training.repository;

import com.smartosc.training.model.Category;
import com.smartosc.training.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CustomRepository<Product, Long> {

    @Query("SELECT p from Product p WHERE p.status = 1 ORDER BY p.createdAt DESC")
    List<Product> findAllByStatusOrderByCreatedAtDesc(Integer status);

    Product findByIdAndStatus(Long id, Integer status);

    List<Product> findAllByStatusAndCategories(Integer status, Category category);

    List<Product> findAllByStatusAndCategoriesIn(Integer status, List<Category> categories);

    @Query("select count(id) from Product ")
    Integer findCount();
}
