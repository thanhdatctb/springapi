package com.smartosc.training.util.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * hanoi-java-fresher-proj
 *
 * @author DuongTrong
 * @created_at 09/01/2020 - 1:30 PM
 * @created_by DuongTrong
 * @since 09/01/2020
 */

@Getter
@Setter
public class EcommerceAPIResponsePagination<T> extends EcommerceAPIResponse<T> {

    private APIResponseDataPagination pagination;

    public EcommerceAPIResponsePagination() {
    }

    public EcommerceAPIResponsePagination(APIResponseDataPagination pagination) {
        this.pagination = pagination;
    }

    public EcommerceAPIResponsePagination(int status, String message, T body, APIResponseDataPagination pagination) {
        super(status, message, body);
        this.pagination = pagination;
    }

    public EcommerceAPIResponsePagination(int status, String message, APIResponseDataPagination pagination) {
        super(status, message);
        this.pagination = pagination;
    }
}
