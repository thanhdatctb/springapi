package com.smartosc.training.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * fres-parent
 *
 * @author trangduong
 * @version 1.0
 * @create_by trangduong
 * @created_date 12/01/2020 - 12:48 PM
 * @since 12/01/2020
 */

@Getter
@Setter
public class ErrorDetails {

    private int status;
    private String message;

    public ErrorDetails(int status, String message) {
        this.status = status;
        this.message = message;
    }
}
