package com.smartosc.training.util;

/**
 * fres-parent
 *
 * @author DuongTrong
 * @created_at 10/01/2020 - 2:41 PM
 * @created_by DuongTrong
 * @since 10/01/2020
 */
public class MessageConstants {

    // success
    public static final String SUCCESS = "Success.";
    public static final String CREATE_SUCCESS = "Create success.";
    public static final String HELLO_BOSS = "Hello Boss...";
    public static final String SUCCESS_ORDER = "Save order success";

    //bad request
    public static final String ITEM_NOT_MATCH = "Item not match";
    public static final String ITEM_NOT_ACCEPT = "Order not accept for you";


    // not found
    public static final String PRODUCT_NOTFOUND = "Product not found.";
    public static final String CATEGORY_NOTFOUND = "Category not found.";
    public static final String ACCOUNT_NOTFOUND = "Account not found.";
    public static final String ACCOUNT_ID_NOTFOUND = "Id account not found.";
    public static final String ORDER_NOTFOUND = "Order not found";

    // null
    public static final String NULL_POINTER = "Null pointer exception.";

    // unauthorized
    public static final String ACCOUNT_ACCESS_DENIED = "Account access denied.";

    // already exists
    public static final String EMAIL_EXIST = "Email already exists.";
    public static final String USER_EXIST = "Username already exists.";
    public static final String PHONE_EXIST = "Phone already exists.";

    // matches
    public static final String PASSWORD_INCORRECT = "Password incorrect.";
    public static final String PASSWORD_MATCHES = "Old password not matches.";
    public static final String PASSWORD_UPDATE_FALSE = "Update password false.";
    public static final String PASSWORD_UPDATE_SUCCESS = "Old password not matches.";

    // param query page
    public static final String PASS_ID = "id";
    public static final String PASS_USER = "username";
    public static final String PASS_EMAIL = "email";
    public static final String PASS_PHONE = "phone";
    public static final String PASS_NAME = "name";
    public static final String PASS_DESCRIPTION = "description";
    public static final String PASS_TITLE = "title";
    public static final String PASS_IN = "in";

    // param condition
    public static final String PASS_STATUS = "status";
    public static final String PASS_CREATE = "createdAt";
    public static final String PASS_ORDER_BY = "orderBy";
    public static final String PASS_DES = "desc";
    public static final String PASS_OPERATION = ":";

    // param key and default
    public static final String PASS_SEARCH = "search";
    public static final String PASS_VALUE_DEFAULT_PAGE = "1";
    public static final String PASS_VALUE_DEFAULT_LIMIT = "10";

}
