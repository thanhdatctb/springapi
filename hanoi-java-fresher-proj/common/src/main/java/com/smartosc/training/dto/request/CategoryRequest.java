package com.smartosc.training.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * fres-parent
 *
 * @author Administrator
 * @created_at 09/01/2020 - 12:01 PM
 * @created_by Administrator
 * @since 09/01/2020
 */
@Setter
@Getter
public class CategoryRequest {

    @JsonProperty("name")
    private String name;
}
