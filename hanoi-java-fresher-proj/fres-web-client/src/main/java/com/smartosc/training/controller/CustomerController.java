package com.smartosc.training.controller;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.smartosc.training.dto.request.UserRequest;
import com.smartosc.training.dto.response.BaseResponse;
import com.smartosc.training.dto.response.UserResponse;
import com.smartosc.training.exception.APIException;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.services.SMTPService;
import com.smartosc.training.springrest.RestTemplateService;
import com.smartosc.training.util.MailContentBuilder;

@Controller
public class CustomerController {

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private ModelMapper modelMapper;

    private HttpHeaders headers = new HttpHeaders();

    @Value("${services.apiPath}")
    private String apiPath;

    @GetMapping("/userDetail")
    private String userDetail(HttpServletRequest request, @RequestParam String username, Model model) {
        String url = apiPath + "/user/" + username;
        try {
            headers.add("Authorization", SecurityUtils.getJWTToken(request));
            BaseResponse<UserResponse> response = restTemplateService.getObject(url, headers, null);
            String result = modelMapper.map(response.getBody(), String.class);
            model.addAttribute("message", result);
            return "user-profile";
        } catch (APIException apiE) {
            model.addAttribute("error", apiE.getMessage());
            return "index";
        }
    }

    @GetMapping("/userUpdate")
    private String userUpdateForm(HttpServletRequest request, @ModelAttribute String username, Model model) {
        String url = apiPath + "/user/" + username;
        try {
            headers.add("Authorization", SecurityUtils.getJWTToken(request));
            BaseResponse<UserResponse> response = restTemplateService.getObject(url, headers, null);
            String result = modelMapper.map(response.getBody(), String.class);
            model.addAttribute("message", result);
            model.addAttribute("userDetail", response.getBody());
            model.addAttribute("userUpdate", new UserRequest());
            return "userUpdateForm";
        } catch (APIException apiE) {
            model.addAttribute("error", apiE.getMessage());
            return "index";
        }
    }

    @PostMapping("/userUpdate")
    private String userUpdate(HttpServletRequest request, @ModelAttribute UserRequest userUpdate, Model model) {
        String url = apiPath + "/user/" + userUpdate.getUsername();
        try {
            headers.add("Authorization", SecurityUtils.getJWTToken(request));
            BaseResponse<UserResponse> response = restTemplateService.updateObject(url, headers, userUpdate);
            String result = modelMapper.map(response.getBody(), String.class);
            model.addAttribute("message", result);
            return "userDetail";
        } catch (APIException apiE) {
            model.addAttribute("error", apiE.getMessage());
            return "index";
        }
    }

    @GetMapping("/user-profile")
    public String profile(Model model, HttpServletRequest request) {
        String url = apiPath + "/customer/profile";
        headers.add("Authorization", SecurityUtils.getJWTToken(request));
        BaseResponse<UserResponse> response = restTemplateService.getObject(url, headers, null);
        UserResponse user = modelMapper.map(response.getBody(), UserResponse.class);
        model.addAttribute("response", user);
        return "user-profile";
    }
}
