package com.smartosc.training.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.smartosc.training.util.annotation.EmailConstraint;
import com.smartosc.training.util.enumm.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "users")
public class User extends DateAudit {

    @Id
    @Column(name = "user_id")
    @JsonProperty("user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @JsonProperty("full_name")
    @Column(name = "fullName")
    private String fullName;

    @NotNull
    @JsonProperty("username")
    @Column(name = "username")
    private String username;

    @Basic
    @NotNull
    @EmailConstraint
    @JsonProperty("email")
    @Column(name = "email")
    private String email;

    @NotNull
    @JsonProperty("password")
    @Column(name = "password")
    private String password;

    @NotNull
    @JsonProperty("phone")
    @Column(name = "phone")
    private String phone;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    private Integer status;

    public User(String fullName, String username, String email, String password, String phone) {
        this.fullName = fullName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.status = Status.ACTIVE.getValue();
    }

    public User() {
        this.status = Status.ACTIVE.getValue();
    }
}
