package com.smartosc.training.controller;

import com.smartosc.training.dto.request.ProductRequest;
import com.smartosc.training.dto.response.CategoryResponse;
import com.smartosc.training.dto.response.ProductResponse;
import com.smartosc.training.security.SecurityUtils;
import com.smartosc.training.service.RestTemplateService;
import com.smartosc.training.util.response.EcommerceAPIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * fres-parent
 *
 * @author DuongTrong
 * @created_at 14/01/2020 - 9:22 PM
 * @created_by DuongTrong
 * @since 14/01/2020
 */

@Controller
@RequestMapping("/products")
public class ProductController {

    private HttpHeaders httpHeaders = new HttpHeaders();

    private ParameterizedTypeReference<EcommerceAPIResponse<ProductResponse>> typeReference =
            new ParameterizedTypeReference<EcommerceAPIResponse<ProductResponse>>() {
            };

    private ParameterizedTypeReference<EcommerceAPIResponse<List<CategoryResponse>>> typeReferences =
            new ParameterizedTypeReference<EcommerceAPIResponse<List<CategoryResponse>>>() {
            };

    @Value("${services.path}")
    private String API_URL;

    @Value("${auth}")
    private String auth;

    @Autowired
    private RestTemplateService restTemplateService;

    @GetMapping
    public String list(Model model) {
        ParameterizedTypeReference<EcommerceAPIResponse<List<ProductResponse>>> typeReferences =
                new ParameterizedTypeReference<EcommerceAPIResponse<List<ProductResponse>>>() {
                };
        List<ProductResponse> productResponseList = restTemplateService.getObjects(API_URL + "/products",
                HttpMethod.GET, null, null, typeReferences);
        model.addAttribute("productResponseList", productResponseList);
        return "product/data";
    }

    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        ProductResponse productResponse = restTemplateService.getObjects(API_URL + "/products/" + id,
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("products", productResponse);
        return "detail";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute("productRequest") @Valid ProductRequest productRequest, Model model, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("productRequest", productRequest);
            return "product/create";
        }
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        restTemplateService.getObjects(API_URL + "/products/create", HttpMethod.POST, httpHeaders, productRequest, typeReference);
        return "redirect:/products";
    }

    @GetMapping("/create")
    public String createO(@ModelAttribute("productRequest") @Valid ProductRequest productRequest, Model model, BindingResult bindingResult, HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("productRequest", productRequest);
            return "product/create";
        }
        model.addAttribute("productRequest", productRequest);
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));

        List<CategoryResponse> categoryResponses = restTemplateService.getObjects(API_URL + "/categories",
                HttpMethod.GET, null, null, typeReferences);
        model.addAttribute("categoryResponses", categoryResponses);
        return "product/create";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
        if (id == null) {
            return "Lỗi rồi";
        }

        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        List<CategoryResponse> categoryResponses = restTemplateService.getObjects(API_URL + "/categories",
                HttpMethod.GET, null, null, typeReferences);
        model.addAttribute("categoryResponses", categoryResponses);
        ProductResponse product = restTemplateService.getObjects(API_URL + "/products/" + id,
                HttpMethod.GET, httpHeaders, null, typeReference);
        model.addAttribute("product", product);
        return "product/edit";
    }

    @PostMapping("/edit/{id}")
    public String editO(@ModelAttribute("product") ProductRequest product, @PathVariable("id") Long id, HttpServletRequest request) {
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        restTemplateService.getObjects(API_URL + "/products/" + id, HttpMethod.PUT, httpHeaders, product, typeReference);
        return "redirect:/products";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id, ProductRequest product, HttpServletRequest request) {
        httpHeaders.add(auth, SecurityUtils.getJWTToken(request));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        restTemplateService.getObjects(API_URL + "/products/" + id, HttpMethod.DELETE, httpHeaders, product, typeReference);
        return "redirect:/products";
    }
}
