package com.smartosc.training.service;

import com.smartosc.training.dto.request.CurrentUserRequest;
import com.smartosc.training.dto.request.UserRequest;
import com.smartosc.training.exception.CustomRuntimeException;
import com.smartosc.training.model.Role;
import com.smartosc.training.model.User;
import com.smartosc.training.repository.RoleRepository;
import com.smartosc.training.repository.UserRepository;
import com.smartosc.training.util.MessageConstants;
import com.smartosc.training.util.enumm.RoleName;
import com.smartosc.training.util.enumm.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User registerAndMerge(User user, UserRequest userRequest) {
        defineUser(user,
                userRequest.getFullName(),
                userRequest.getUsername(),
                userRequest.getEmail(),
                userRequest.getPhone());

        user.setPassword(userRequest.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER);
        user.setRoles(Collections.singleton(userRole));
        user.setStatus(Status.ACTIVE.getValue());
        return userRepository.save(user);
    }

    @Override
    public User registerAdmin(User user, UserRequest userRequest) {
        user.setFullName(userRequest.getFullName());
        user.setUsername(userRequest.getUsername());
        user.setEmail(userRequest.getEmail());
        user.setPhone(userRequest.getPhone());
        user.setStatus(Status.ACTIVE.getValue());

        user.setPassword(userRequest.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByName(RoleName.ROLE_ADMIN);
        user.setRoles(Collections.singleton(userRole));

        return userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsernameOrPhone(String username, String phone) {
        return userRepository.findByUsernameOrPhone(username, phone);
    }

    @Override
    public boolean checkRoleByUser(User user, RoleName roleName) {
        Set<Role> roles = user.getRoles();
        Role role = roleRepository.findByName(roleName);
        return roles.contains(role);
    }

    @Override
    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public Integer findCount() {
        return userRepository.findCount();
    }

    @Override
    public boolean existsByPhone(String phone) {
        return userRepository.existsByPhone(phone);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAllByStatusAndRoles(Status.ACTIVE.getValue(), roleRepository.findByName(RoleName.ROLE_USER));
    }

    @Override
    public User getUserAuth() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String username = authentication.getName();
            return userRepository.findByUsername(username);
        }
        return null;
    }

    @Override
    public User findById(Long id) {
        if (CollectionUtils.isEmpty(Collections.singleton(id))) {
            throw new CustomRuntimeException(MessageConstants.NULL_POINTER);
        }
        return userRepository.findByIdAndStatus(id, Status.ACTIVE.getValue());
    }

    @Override
    public User update(User user, CurrentUserRequest currentUserRequest) {
        defineUser(user,
                currentUserRequest.getFullName(),
                currentUserRequest.getUsername(),
                currentUserRequest.getEmail(),
                currentUserRequest.getPhone());

        if (currentUserRequest.getRoles().size() > 0 && currentUserRequest.getRoles() != null) {
            List<Role> roleList = roleRepository.findAllById(currentUserRequest.getRoles());
            Set<Role> roles = new HashSet<>(roleList);
            user.setRoles(roles);
        }
        return userRepository.save(user);
    }

    private void defineUser(User user, String fullName, String username, String email, String phone) {
        if (fullName != null) user.setFullName(fullName);
        if (username != null) user.setUsername(username);
        if (email != null) user.setEmail(email);
        if (phone != null) user.setPhone(phone);
    }

    @Override
    public void delete(User user) {
        user.setStatus(Status.DEACTIVE.getValue());
        userRepository.save(user);
    }

    @Override
    public boolean checkUserPassword(String oldPassword, User user) {
        return passwordEncoder.matches(oldPassword, user.getPassword());
    }

    @Override
    public boolean updatePassword(String password, User user) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
        return true;
    }

    @Override
    public Page<User> usersWithPaginate(Specification specification, int page, int limit) {
        return userRepository.findAll(specification, PageRequest.of(page - 1, limit));
    }

    @Override
    public List<String> getUsernameList() {
        return userRepository.getUsernames();
    }
}
